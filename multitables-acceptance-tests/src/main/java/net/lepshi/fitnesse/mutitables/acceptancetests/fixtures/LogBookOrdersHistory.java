package net.lepshi.fitnesse.mutitables.acceptancetests.fixtures;

import com.google.gson.Gson;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

import static com.google.common.base.Preconditions.checkState;


@Slf4j
public class LogBookOrdersHistory {

    private final Gson gson = new Gson();

    public String doJson(String json) {
        BookOrdersHistory obj = gson.fromJson(json, BookOrdersHistory.class);

        LOG.info("OBJ:\n{}", obj);

        String discriminator = obj.orders.get(0).shipping.name;
        checkState(!discriminator.contains("err0"), "Fixture error");
        return discriminator;
    }

    @Data
    static class BookOrdersHistory {

        private List<Order> orders;

        @Data
        static class Order {
            private boolean  isGift;
            private Shipping shipping;
            private Book[]   books;
        }

        @Data
        static class Shipping {
            private String name;
            private String address;
            private double cost;
        }

        @Data
        static class Book {
            private String title;
            private String author;
            private int    quantity;
            private double price;
        }
    }
}
