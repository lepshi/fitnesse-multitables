package net.lepshi.fitnesse.mutitables.acceptancetests.fixtures;

import com.google.gson.Gson;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;


@Slf4j
public class FakeAssert {

    private final Gson gson = new Gson();

    public String doJson(String json) {
        FakeAssertData assertData = gson.fromJson(json, FakeAssertData.class);

        if (assertData.fakeResponse.contains("err0")) {
            throw new RuntimeException("Fixture error");
        }
        return assertData.fakeResponse;
    }


    @Data
    private static class FakeAssertData {

        private String fakeResponse;
        private String comment;
    }
}
