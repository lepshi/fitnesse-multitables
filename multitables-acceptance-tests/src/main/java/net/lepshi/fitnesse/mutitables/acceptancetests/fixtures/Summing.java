package net.lepshi.fitnesse.mutitables.acceptancetests.fixtures;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

import static java.util.Arrays.asList;


@Getter
@Setter
public class Summing {

    private int a;
    private int b;

    public int result() {
        return a + b;
    }
}
