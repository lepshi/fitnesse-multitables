package net.lepshi.fitnesse.mutitables.plugin.common;

import fitnesse.testsystems.slim.Table;
import fitnesse.testsystems.slim.results.SlimExceptionResult;
import fitnesse.testsystems.slim.results.SlimTestResult;

import static com.google.common.base.Preconditions.checkArgument;


public class SubTable extends UnsupportedOperationsTable {

    private final Table baseTable;
    private final int   firstCol;
    private final int   lastCol;
    private final int   firstRow;
    private final int   lastRow;

    public SubTable(Table baseTable, int firstCol, int lastCol, int firstRow, int lastRow) {
        this.baseTable = baseTable;
        this.firstCol = firstCol;
        this.lastCol = Math.min(lastCol, Integer.MAX_VALUE - 1);
        this.firstRow = firstRow;
        this.lastRow = Math.min(lastRow, Integer.MAX_VALUE - 1);

        checkArgument(firstCol >= 0);
        checkArgument(lastCol >= 0);
        checkArgument(firstRow >= 0);
        checkArgument(lastRow >= 0);

        checkArgument(firstRow < baseTable.getRowCount());
        checkArgument(lastRow < baseTable.getRowCount());
        checkTopLeftTableCell();
    }

    private void checkTopLeftTableCell() {
        try {
            baseTable.getCellContents(firstCol, firstRow);
        } catch (Exception e) {
            throw new IllegalArgumentException("No cell exists in base [col:" + firstCol + ", row:" + firstRow + "]", e);
        }
    }


    @Override
    public boolean isTearDown() {
        return baseTable.isTearDown();
    }

    @Override
    public String getCellContents(int col, int row) {
        checkArgument(firstCol + col <= lastCol);
        checkArgument(firstRow + row <= lastRow);

        if (isRowMissing(row) && col == 0) {
            return "";
        }
        return baseTable.getCellContents(col + firstCol, row + firstRow);
    }

    @Override
    public int getRowCount() {
        return lastRow - firstRow + 1;
    }

    @Override
    public int getColumnCountInRow(int row) {
        if (isRowMissing(row)) {
            return 1;
        }
        int baseColCount = baseTable.getColumnCountInRow(row + firstRow);
        int subColCountUpperBound = lastCol - firstCol + 1;
        return Math.min(baseColCount - firstCol, subColCountUpperBound);
    }

    @Override
    public void substitute(int col, int row, String content) {
        if (isRowMissing(row) && col == 0) {
            return;
        }
        baseTable.substitute(col + firstCol, row + firstRow, content);
    }

    @Override
    public void updateContent(int col, int row, SlimTestResult testResult) {
        if (isRowMissing(row) && col == 0) {
            return;
        }
        baseTable.updateContent(col + firstCol, row + firstRow, testResult);
    }

    @Override
    public void updateContent(int col, int row, SlimExceptionResult exceptionResult) {
        if (isRowMissing(row) && col == 0) {
            return;
        }
        baseTable.updateContent(col + firstCol, row + firstRow, exceptionResult);
    }


    private boolean isRowMissing(int row) {
        return baseTable.getColumnCountInRow(row + firstRow) <= firstCol;
    }
}