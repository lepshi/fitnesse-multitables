package net.lepshi.fitnesse.mutitables.plugin.tables;

import fitnesse.testsystems.slim.tables.SlimAssertion;

import java.util.List;


interface AssertionsCustomizer {

    List<SlimAssertion> customize(List<SlimAssertion> assertions);

    default AssertionsCustomizer followedBy(AssertionsCustomizer nextCombiner) {
        return assertions -> {
            final List<SlimAssertion> firstCombinerAssertions = this.customize(assertions);
            return nextCombiner.customize(firstCombinerAssertions);
        };
    }
}
