package net.lepshi.fitnesse.mutitables.plugin;

import java.util.concurrent.atomic.AtomicBoolean;

public class TableException extends RuntimeException {

    private final TableDataHighlighter tableDataHighlighter;
    private final AtomicBoolean        hasAlreadyHighlighted = new AtomicBoolean();


    public TableException(TableDataHighlighter tableDataHighlighter, String message) {
        super(message);
        this.tableDataHighlighter = tableDataHighlighter;
    }

    public TableException(TableDataHighlighter tableDataHighlighter, String message, Throwable cause) {
        super(message, cause);
        this.tableDataHighlighter = tableDataHighlighter;
    }


    public void highlightTableData() {
        boolean notYetHighlighted = !hasAlreadyHighlighted.getAndSet(true);
        if (notYetHighlighted) {
            tableDataHighlighter.highlight();
        }
    }
}
