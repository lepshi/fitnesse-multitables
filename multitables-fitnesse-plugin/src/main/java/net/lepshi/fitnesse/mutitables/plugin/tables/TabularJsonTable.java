package net.lepshi.fitnesse.mutitables.plugin.tables;

import fitnesse.slim.instructions.Instruction;
import fitnesse.testsystems.ExecutionResult;
import fitnesse.testsystems.TestResult;
import fitnesse.testsystems.slim.SlimTestContext;
import fitnesse.testsystems.slim.Table;
import fitnesse.testsystems.slim.results.SlimExceptionResult;
import fitnesse.testsystems.slim.tables.SlimAssertion;
import fitnesse.testsystems.slim.tables.SlimExpectation;
import fitnesse.testsystems.slim.tables.TableTable;
import lombok.extern.slf4j.Slf4j;
import net.lepshi.fitnesse.mutitables.plugin.TableException;
import net.lepshi.fitnesse.mutitables.plugin.common.StringTable;
import net.lepshi.fitnesse.mutitables.plugin.common.SubTable;
import net.lepshi.fitnesse.mutitables.plugin.common.TableDataHighlighterFactory;
import net.lepshi.fitnesse.mutitables.plugin.data.Rows;
import net.lepshi.fitnesse.mutitables.plugin.data.TabularData;

import java.util.List;
import java.util.regex.Matcher;

import static fitnesse.testsystems.ExecutionResult.IGNORE;
import static fitnesse.testsystems.slim.results.SlimExceptionResult.EXCEPTION_MESSAGE_PATTERN;
import static java.lang.String.format;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Objects.isNull;
import static org.apache.commons.lang3.StringUtils.isBlank;

@Slf4j
public class TabularJsonTable extends TableTable {

    private final TableDataHighlighterFactory highlighterFactory;
    private final Rows                        tableContentRows;
    private final Rows                        dataRows;

    public TabularJsonTable(Table table, String id, SlimTestContext testContext) {
        super(table, id, testContext);
        this.highlighterFactory = new TableDataHighlighterFactory(table);
        this.tableContentRows = Rows.range(1, table.getRowCount() - 1);
        this.dataRows = tableContentRows.exceptFirst();
    }


    @Override
    protected String getTableType() {
        return "tabularJsonTable";
    }

    @Override
    public List<SlimAssertion> getAssertions() {
        try {
            SlimAssertion make = constructFixture(getFixtureName());

            Instruction doJson = callFunction(getTableName(), "doJson", tableDataAsJson());
            SlimAssertion call = super.makeAssertion(doJson, new DoJsonExpectation());

            return asList(make, call);

        } catch (TableException e) {
            getTestContext().incrementErroredTestsCount();
            LOG.debug("Error getting assertions: {}", e.getMessage(), e);
            e.highlightTableData();
            highlighterFactory.rows(IGNORE, dataRows).highlight();
            return emptyList();
        }
    }

    private String tableDataAsJson() {
        final Table dataTable = new SubTable(getTable(),
                                             0,
                                             Integer.MAX_VALUE,
                                             tableContentRows.first,
                                             tableContentRows.last);
        LOG.trace("Parsing tabular JSON data:\n{}", StringTable.from(dataTable));
        final String json = new TabularData(dataTable).asJson();
        LOG.trace("Parsed JSON is: {}", json);
        return json;
    }


    private class DoJsonExpectation implements SlimExpectation {

        @Override
        public TestResult evaluateExpectation(Object tableResult) {
            final FixtureResult fixtureResult = FixtureResult.parseFrom((String) tableResult);
            LOG.debug("(({}) {}).doJson() returned [{}]", getFixtureName(), getTableName(), fixtureResult);
            ExecutionResult executionResult = fixtureResult.asExecutionResult();
            highlighterFactory.rows(executionResult,
                                    dataRows,
                                    messageWithPrefix(fixtureResult.code, fixtureResult.message))
                              .highlight();
            getTestContext().increment(executionResult);
            return fixtureResult.asTestResult();
        }

        @Override
        public SlimExceptionResult evaluateException(SlimExceptionResult exceptionResult) {
            String errorToHighlight = exceptionResult.getException();
            if (isSlimError(exceptionResult)) {
                LOG.debug("(({}) {}).doJson() returned Slim ERROR: {}",
                          getFixtureName(),
                          getTableName(),
                          exceptionResult.getException());
                errorToHighlight = exceptionResult.getMessage();
            }

            highlighterFactory.rows(ExecutionResult.FAIL,
                                    dataRows,
                                    messageWithPrefix("ERROR", errorToHighlight))
                              .highlight();

            getTestContext().incrementErroredTestsCount();
            return exceptionResult;
        }

        private boolean isSlimError(SlimExceptionResult exceptionResult) {
            final String exceptionMessage = exceptionResult.getException();
            Matcher matcher = EXCEPTION_MESSAGE_PATTERN.matcher(exceptionMessage);
            return matcher.find();
        }

        private String messageWithPrefix(String code, String message) {
            if (isNull(message)) {
                return null;
            }
            if (isBlank(code)) {
                code = "none";
            }
            return format("%s[%s]: %s",
                          getFixtureName(),
                          code.toUpperCase(),
                          message);
        }

    }

}
