package net.lepshi.fitnesse.mutitables.plugin.tables;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@Getter
class EachTestSteps {

    private final List<ActionCombination> beforeSteps = new ArrayList<>();
    private final List<ActionCombination> afterSteps  = new ArrayList<>();

    void addSteps(ActionCombination.CombinationType type, List<ActionCombination> steps) {
        switch (type) {
            case BEFORE_EACH_TEST_STEP:
                beforeSteps.addAll(steps);
                break;

            case AFTER_EACH_TEST_STEP:
                afterSteps.addAll(steps);
                break;

            default:
                throw new IllegalArgumentException("Invalid (before/after) step type: " + type);
        }
    }
}
