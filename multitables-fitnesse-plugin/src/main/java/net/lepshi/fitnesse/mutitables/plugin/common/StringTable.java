package net.lepshi.fitnesse.mutitables.plugin.common;

import fitnesse.testsystems.slim.Table;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static com.google.common.base.Joiner.on;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;
import static java.util.stream.IntStream.range;
import static org.apache.commons.lang3.StringUtils.splitPreserveAllTokens;


@EqualsAndHashCode(callSuper = false)
@RequiredArgsConstructor
public class StringTable extends UnsupportedOperationsTable {

    private final List<List<String>> table;


    public static StringTable from(String... rows) {
        return new StringTable(Stream.of(rows)
                                     .map(StringTable::rowToCells)
                                     .collect(toList()));
    }

    public static StringTable from(Table other) {
        List<List<String>> table = new ArrayList<>();
        for (int rowIdx = 0; rowIdx < other.getRowCount(); rowIdx++) {
            List<String> row = new ArrayList<>();
            for (int colIdx = 0; colIdx < other.getColumnCountInRow(rowIdx); colIdx++) {
                row.add(other.getCellContents(colIdx, rowIdx));
            }
            table.add(row);
        }
        return new StringTable(table);
    }

    public static StringTable empty(int cols, int rows) {
        final List<List<String>> empty =
                range(0, rows).mapToObj(
                        row -> range(0, cols).mapToObj(col -> "")
                                             .collect(toList()))
                              .collect(toList());
        return new StringTable(empty);
    }

    private static List<String> rowToCells(String row) {
        String[] parts = splitPreserveAllTokens(row, "|");
        String[] cells = Arrays.copyOfRange(parts, 1, parts.length - 1);
        return Stream.of(cells)
                     .map(String::trim)
                     .collect(toList());
    }


    @Override
    public String getCellContents(int col, int row) {
        return table.get(row)
                    .get(col);
    }

    @Override
    public int getRowCount() {
        return table.size();
    }

    @Override
    public int getColumnCountInRow(int row) {
        return table.get(row)
                    .size();
    }

    @Override
    public void substitute(int col, int row, String content) {
        table.get(row)
             .set(col, content);
    }

    @Override
    public int addRow(List<String> extraRow) {
        table.add(extraRow);
        return getRowCount();
    }


    @Override
    public String toString() {
        return table.stream()
                    .map(row -> on('|').join(row))
                    .collect(joining("|\n|", "|", "|"));
    }
}
