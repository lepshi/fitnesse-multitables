package net.lepshi.fitnesse.mutitables.plugin.data;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

import java.util.stream.IntStream;

import static com.google.common.base.Preconditions.checkState;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
@ToString
public class Rows {

    public final int first;
    public final int last;

    public static Rows range(int first, int last) {
        return new Rows(first, last);
    }

    public static Rows single(int row) {
        return new Rows(row, row);
    }

    public Rows exceptFirst() {
        checkState(count() > 0);
        return Rows.range(first + 1, last);
    }

    public int count() {
        return Math.max(0,
                        last - first + 1);
    }

    public IntStream stream() {
        return IntStream.rangeClosed(first, last);
    }
}
