package net.lepshi.fitnesse.mutitables.plugin.tables;

import fitnesse.testsystems.ExecutionResult;
import fitnesse.testsystems.TestResult;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Stream;

import static fitnesse.testsystems.ExecutionResult.ERROR;
import static fitnesse.testsystems.ExecutionResult.FAIL;
import static fitnesse.testsystems.ExecutionResult.IGNORE;
import static fitnesse.testsystems.ExecutionResult.PASS;
import static java.util.function.Function.identity;


@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
class ResultsPicker {

    private final List<? extends TestResult> results;

    private Function<ExecutionResult, ExecutionResult> adjustment = identity();


    static ResultsPicker pickFromResults(List<? extends TestResult> results) {
        return new ResultsPicker(results);
    }


    ExecutionResult worst() {
        return Stream.of(ERROR, FAIL, IGNORE)
                     .filter(this::isAmongTestResults)
                     .map(adjustment)
                     .findFirst()
                     .orElse(PASS);
    }

    private boolean isAmongTestResults(ExecutionResult execResult) {
        return results.stream()
                      .map(TestResult::getExecutionResult)
                      .anyMatch(execResult::equals);
    }

    ResultsPicker adjusted(Function<ExecutionResult, ExecutionResult> adjustment) {
        this.adjustment = adjustment;
        return this;
    }
}
