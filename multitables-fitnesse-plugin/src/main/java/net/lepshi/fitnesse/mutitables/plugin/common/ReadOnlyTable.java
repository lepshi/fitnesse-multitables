package net.lepshi.fitnesse.mutitables.plugin.common;

import fitnesse.testsystems.TestResult;
import fitnesse.testsystems.slim.Table;
import fitnesse.testsystems.slim.results.SlimExceptionResult;
import fitnesse.testsystems.slim.results.SlimTestResult;
import lombok.RequiredArgsConstructor;


@RequiredArgsConstructor
public class ReadOnlyTable extends UnsupportedOperationsTable {

    private final Table baseTable;

    public static Table readOnly(Table table) {
        return new ReadOnlyTable(table);
    }


    @Override
    public boolean isTearDown() {
        return baseTable.isTearDown();
    }

    @Override
    public String getCellContents(int col, int row) {
        return baseTable.getCellContents(col, row);
    }

    @Override
    public int getRowCount() {
        return baseTable.getRowCount();
    }

    @Override
    public int getColumnCountInRow(int row) {
        return baseTable.getColumnCountInRow(row);
    }


    @Override
    public void substitute(int col, int row, String content) {
        //ignore
    }

    @Override
    public void updateContent(int col, TestResult testResult) {
        //ignore
    }

    @Override
    public void updateContent(int col, int row, SlimTestResult testResult) {
        //ignore
    }

    @Override
    public void updateContent(int col, int row, SlimExceptionResult exceptionResult) {
        //ignore
    }

}
