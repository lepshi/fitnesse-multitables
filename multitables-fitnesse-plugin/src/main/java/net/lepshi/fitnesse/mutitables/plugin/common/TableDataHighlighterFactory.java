package net.lepshi.fitnesse.mutitables.plugin.common;

import fitnesse.testsystems.ExecutionResult;
import fitnesse.testsystems.slim.Table;
import fitnesse.testsystems.slim.results.SlimExceptionResult;
import fitnesse.testsystems.slim.results.SlimTestResult;
import net.lepshi.fitnesse.mutitables.plugin.TableDataHighlighter;
import net.lepshi.fitnesse.mutitables.plugin.data.Rows;
import lombok.RequiredArgsConstructor;

import javax.annotation.Nullable;
import java.util.function.Function;

import static fitnesse.testsystems.ExecutionResult.FAIL;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static net.lepshi.fitnesse.mutitables.plugin.common.TableCells.cellsOf;

@RequiredArgsConstructor
public class TableDataHighlighterFactory {

    public static final Function<ExecutionResult, ExecutionResult> ADJUSTMENT_FOR_HIGHLIGHTING = result -> {
        if (nonNull(result)) {
            switch (result) {
                case ERROR:
                    return FAIL;
            }
        }
        return result;
    };

    public static final ExecutionResult NONE_EXEC_RESULT = null;

    private final Table table;


    public TableDataHighlighter cell(ExecutionResult executionResult, int col, int row) {
        return cell(executionResult, col, row, null);
    }

    public TableDataHighlighter cell(ExecutionResult executionResult, int col, int row, @Nullable String newCellText) {
        return new CellResultHighlighter(table, col, row, getCellResult(executionResult, newCellText));
    }

    public TableDataHighlighter row(ExecutionResult executionResult, int row) {
        return row(executionResult, row, null);
    }

    public TableDataHighlighter row(ExecutionResult executionResult, int row, @Nullable String rowMessage) {
        return rows(executionResult, Rows.single(row), rowMessage);
    }

    public TableDataHighlighter rows(ExecutionResult executionResult, Rows rows) {
        return rows(executionResult, rows, null);
    }

    public TableDataHighlighter rows(ExecutionResult executionResult, Rows rows, @Nullable String lastRowMessage) {
        return new TableRowsHighlighter(table, rows, getCellResult(executionResult), lastRowMessage);
    }

    public TableDataHighlighter table(ExecutionResult executionResult) {
        return rows(executionResult, allTableRows());
    }

    public TableDataHighlighter table(ExecutionResult executionResult, @Nullable String tableMessage) {
        return rows(executionResult, allTableRows(), tableMessage);
    }

    public TableDataHighlighter cellLabel(int col, int row, String labelMsg) {
        return new CellLabelHighlighter(table, col, row, labelMsg);
    }


    private Rows allTableRows() {
        return Rows.range(0, table.getRowCount() - 1);
    }

    private static SlimTestResult getCellResult(ExecutionResult executionResult) {
        return getCellResult(executionResult, null);
    }

    private static SlimTestResult getCellResult(ExecutionResult executionResult, String message) {
        if (executionResult == NONE_EXEC_RESULT) {
            return isNull(message) ? null : SlimTestResult.plain(message);
        }
        switch (executionResult) {
            case FAIL:
                return SlimTestResult.fail(message);
            case IGNORE:
                return SlimTestResult.ignore(message);
            case PASS:
                return SlimTestResult.pass(message);
        }
        throw new IllegalArgumentException("TableDataHiglighter not supported for result of type: " + executionResult.name());
    }


    @RequiredArgsConstructor
    private static class CellResultHighlighter implements TableDataHighlighter {
        private final Table          table;
        private final int            col;
        private final int            row;
        private final SlimTestResult testResult;

        @Override
        public void highlight() {
            if (nonNull(testResult)) {
                table.updateContent(col, row, testResult);
            }
        }
    }

    @RequiredArgsConstructor
    private static class CellLabelHighlighter implements TableDataHighlighter {
        private final Table  table;
        private final int    col;
        private final int    row;
        private final String label;

        @Override
        public void highlight() {
            table.updateContent(col, row, new SlimExceptionResult("tableCellLabel",
                                                                  "message:<<" + label + ">>"));
        }
    }

    @RequiredArgsConstructor
    private static class TableRowsHighlighter implements TableDataHighlighter {
        private final Table          table;
        private final Rows           rows;
        private final SlimTestResult testResult;
        private final String         lastRowMessage;

        @Override
        public void highlight() {
            if (nonNull(testResult)) {
                for (Cell cell : TableCells.cellsOf(table, rows)) {
                    table.updateContent(cell.col, cell.row, testResult);
                }
            }
            if (nonNull(lastRowMessage)) {
                int lastRow = nonNull(rows) ? rows.last
                                            : table.getRowCount() - 1;
                table.updateContent(0, lastRow, new SlimExceptionResult("tableRowsError", lastRowMessage));
            }
        }
    }

}
