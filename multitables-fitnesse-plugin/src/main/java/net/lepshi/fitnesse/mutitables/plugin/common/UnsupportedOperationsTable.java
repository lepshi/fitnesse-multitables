package net.lepshi.fitnesse.mutitables.plugin.common;

import fitnesse.testsystems.TestResult;
import fitnesse.testsystems.slim.Table;
import fitnesse.testsystems.slim.results.SlimExceptionResult;
import fitnesse.testsystems.slim.results.SlimTestResult;

import java.util.List;


class UnsupportedOperationsTable implements Table {

    @Override
    public boolean isTearDown() {
        throw new UnsupportedOperationException();
    }

    @Override
    public String getCellContents(int col, int row) {
        throw new UnsupportedOperationException();
    }

    @Override
    public int getRowCount() {
        throw new UnsupportedOperationException();
    }

    @Override
    public int getColumnCountInRow(int row) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void substitute(int col, int row, String content) {
        throw new UnsupportedOperationException();
    }

    @Override
    public int addRow(List<String> list) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void addColumnToRow(int row, String content) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void appendChildTable(int row, Table table) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void updateContent(int row, TestResult testResult) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void updateContent(int col, int row, SlimTestResult testResult) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void updateContent(int col, int row, SlimExceptionResult exceptionResult) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Table asTemplate(CellContentSubstitution substitution) {
        throw new UnsupportedOperationException();
    }
}