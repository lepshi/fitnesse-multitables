package net.lepshi.fitnesse.mutitables.plugin.tables;

import fitnesse.slim.instructions.Instruction;
import fitnesse.testsystems.TestResult;
import fitnesse.testsystems.slim.results.SlimExceptionResult;
import fitnesse.testsystems.slim.results.SlimTestResult;
import fitnesse.testsystems.slim.tables.SlimAssertion;
import fitnesse.testsystems.slim.tables.SlimExpectation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.reflect.FieldUtils;


@Slf4j
@RequiredArgsConstructor
class ObservedSlimAssertion {


    static SlimAssertion observedAssertion(SlimAssertion assertion, AssertionObserver observer) {
        try {
            final Instruction instruction = (Instruction) FieldUtils.readField(assertion, "instruction", true);
            final SlimExpectation expectation = assertion.getExpectation();

            return new SlimAssertion(instruction,
                                     new ObservedExpectation(expectation, observer));
        } catch (IllegalAccessException e) {
            throw new RuntimeException("Fitnesse dependency version error: SlimAssertion class no longer has field 'instruction'", e);
        }
    }


    @RequiredArgsConstructor
    private static class ObservedExpectation implements SlimExpectation {

        private final    SlimExpectation   expectation;
        private final    AssertionObserver observer;
        private volatile boolean           hasNotified;

        @Override
        public TestResult evaluateExpectation(Object returnValues) {
            final TestResult testResult = expectation.evaluateExpectation(returnValues);
            notifyOnce(testResult);
            return testResult;
        }

        @Override
        public SlimExceptionResult evaluateException(SlimExceptionResult exceptionResult) {
            final TestResult exceptionTestResult = SlimTestResult.error(exceptionResult.hasMessage() ? exceptionResult.getMessage()
                                                                                                     : exceptionResult.getException());
            notifyOnce(exceptionTestResult);
            return expectation.evaluateException(exceptionResult);
        }

        private void notifyOnce(TestResult testResult) {
            if (!hasNotified) {
                hasNotified = true;
                observer.handleTestResult(testResult);
            }
        }
    }


    @FunctionalInterface
    public interface AssertionObserver {
        void handleTestResult(TestResult testResult);
    }
}
