package net.lepshi.fitnesse.mutitables.plugin.data;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import lombok.RequiredArgsConstructor;


class ObjectStructureNodes {

    @RequiredArgsConstructor
    static abstract class Node {
        final Node parent;

        String getFullName() {
            return new NodeNameBuilder(this).buildName();
        }

        abstract <T> T handleBy(NodeHandler<T> handler);
    }

    interface NodeHandler<T> {

        T handle(SimpleNode simpleNode);

        T handle(CompositeNode compositeNode);

        T handle(ArrayNode arrayNode);
    }


    static class SimpleNode extends Node {

        final int    col;
        final String headerName;

        SimpleNode(Node parent, String headerName, int col) {
            super(parent);
            this.col = col;
            this.headerName = headerName;
        }

        @Override
        <T> T handleBy(NodeHandler<T> handler) {
            return handler.handle(this);
        }
    }


    static class CompositeNode extends Node {

        final BiMap<String, Node> children = HashBiMap.create(32);

        CompositeNode(Node parent) {
            super(parent);
        }

        @Override
        <T> T handleBy(NodeHandler<T> handler) {
            return handler.handle(this);
        }
    }


    static class ArrayNode extends CompositeNode {

        static final String PRIMITIVE_CHILD_NAME = "__PRIMITIVE_CHILD_";
        static final String COMPOSITE_CHILD_NAME = "__COMPOSITE_CHILD_";

        ArrayNode(Node parent) {
            super(parent);
        }

        @Override
        <T> T handleBy(NodeHandler<T> handler) {
            return handler.handle(this);
        }
    }

}
