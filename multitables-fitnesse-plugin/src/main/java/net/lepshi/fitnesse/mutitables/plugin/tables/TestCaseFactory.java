package net.lepshi.fitnesse.mutitables.plugin.tables;

import com.google.common.annotations.VisibleForTesting;
import fitnesse.testsystems.ExecutionResult;
import fitnesse.testsystems.TestResult;
import fitnesse.testsystems.slim.SlimTestContext;
import fitnesse.testsystems.slim.Table;
import net.lepshi.fitnesse.mutitables.plugin.common.TableDataHighlighterFactory;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static net.lepshi.fitnesse.mutitables.plugin.common.TableDataHighlighterFactory.ADJUSTMENT_FOR_HIGHLIGHTING;
import static net.lepshi.fitnesse.mutitables.plugin.tables.ResultsPicker.pickFromResults;

@Slf4j
@RequiredArgsConstructor
class TestCaseFactory implements ActionCombinationFactory {

    private final EachTestSteps   eachTestSteps;
    private final SlimTestContext testContext;
    private final AtomicInteger   nextId = new AtomicInteger(0);

    @Override
    public ActionCombination.CombinationType getCombinationType() {
        return ActionCombination.CombinationType.TEST_CASE;
    }

    @Override
    public ActionCombination createActionCombination(Table titleTable,
                                                     Table combinationReportingTable,
                                                     List<Table> actionTables) {
        final String testCaseId = ActionCombination.CombinationType.TEST_CASE.getId() + nextId.getAndIncrement();
        return new ActionCombination(getCombinationType(),
                                     () -> testCaseId,
                                     titleTable,
                                     actionTables,
                                     assertionsCustomizer(titleTable, combinationReportingTable),
                                     testContext);
    }


    @VisibleForTesting
    AssertionsCustomizer assertionsCustomizer(Table titleTable, Table stepsReportingTable) {
        return new StepsAttachingCustomizer(eachTestSteps, stepsReportingTable)
                .followedBy(new AllExpectationsEvaluatedNotifyingCustomizer(testCaseActionsEvaluator(titleTable)));
    }

    @VisibleForTesting
    AllExpectationsEvaluatedNotifyingCustomizer.AllExpectationsEvaluatedCallback testCaseActionsEvaluator(Table titleTable) {
        return actionResults -> evaluateTestCaseActions(titleTable, actionResults);
    }

    private void evaluateTestCaseActions(Table titleTable, List<TestResult> actionResults) {
        final String combinationTitle = titleTable.getCellContents(0, 0);
        final TableDataHighlighterFactory titleHighlighterFactory = new TableDataHighlighterFactory(titleTable);

        final ExecutionResult worstResult = pickFromResults(actionResults).adjusted(ADJUSTMENT_FOR_HIGHLIGHTING)
                                                                          .worst();
        LOG.debug("{} finished: {}", getCombinationType().decorateTitle(combinationTitle), worstResult.name());
        titleHighlighterFactory.table(worstResult).highlight();
    }

}
