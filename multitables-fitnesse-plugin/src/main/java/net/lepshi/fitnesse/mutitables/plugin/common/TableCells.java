package net.lepshi.fitnesse.mutitables.plugin.common;

import fitnesse.testsystems.slim.Table;
import net.lepshi.fitnesse.mutitables.plugin.data.Rows;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.Objects.nonNull;


class TableCells implements Iterable<Cell> {

    private final Table table;
    private final Rows  rows;

    static TableCells cellsOf(Table table) {
        return new TableCells(table, null);
    }

    static TableCells cellsOf(Table table, Rows rows) {
        checkNotNull(rows, "Rows must not be null");
        return new TableCells(table, rows);
    }

    private TableCells(Table table, Rows rows) {
        checkNotNull(table, "Table must not be null");
        this.table = table;
        this.rows = nonNull(rows) ? rows
                                  : Rows.range(0, table.getRowCount() - 1);
    }


    List<Cell> asList() {
        return getCells();
    }

    private List<Cell> getCells() {
        List<Cell> cells = new ArrayList<>();
        for (int row = rows.first; row <= rows.last; row++) {
            for (int col = 0; col < table.getColumnCountInRow(row); col++) {
                cells.add(Cell.cell(col, row));
            }
        }
        return cells;
    }

    @Override
    public Iterator<Cell> iterator() {
        return getCells().iterator();
    }
}
