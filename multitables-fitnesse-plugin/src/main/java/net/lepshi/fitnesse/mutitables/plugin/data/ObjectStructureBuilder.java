package net.lepshi.fitnesse.mutitables.plugin.data;

import com.google.common.annotations.VisibleForTesting;
import fitnesse.testsystems.slim.Table;
import net.lepshi.fitnesse.mutitables.plugin.TableDataHighlighter;
import net.lepshi.fitnesse.mutitables.plugin.TableException;
import net.lepshi.fitnesse.mutitables.plugin.common.TableDataHighlighterFactory;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkState;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static org.apache.commons.lang3.StringUtils.isBlank;


class ObjectStructureBuilder {

    private final BuildingState CHOOSE_NODE_TYPE   = new ChooseNodeTypeState();
    private final BuildingState ADD_SIMPLE_NODE    = new AddSimpleNodeState();
    private final BuildingState ADD_COMPOSITE_NODE = new AddCompositeNodeState();
    private final BuildingState ADD_ARRAY_NODE     = new AddArrayNodeState();

    private final Table                           data;
    private final TableDataHighlighterFactory     highlighterFactory;
    private       ObjectStructureNodes.SimpleNode previousSimpleNode;
    private       int                             currentCol;
    private       String                          currentNodeHeaderName;


    ObjectStructureBuilder(Table data) {
        this.data = data;
        this.highlighterFactory = new TableDataHighlighterFactory(data);
    }


    ObjectStructureNodes.Node buildObjectRoot() {
        final ObjectStructureNodes.CompositeNode objectRoot = new ObjectStructureNodes.CompositeNode(null);
        while (currentCol < data.getColumnCountInRow(0)) {
            String nodeSpec = data.getCellContents(currentCol, 0);
            currentNodeHeaderName = nodeSpec;
            if (!isBlank(nodeSpec)) {
                buildNodeFromRoot(objectRoot, nodeSpec);
            }
            currentCol++;
        }
        return objectRoot;
    }

    @VisibleForTesting
    void buildNodeFromRoot(ObjectStructureNodes.CompositeNode objectRoot, String nodeSpec) {
        try {
            CHOOSE_NODE_TYPE.handle(objectRoot, null, nodeSpec);

        } catch (RuntimeException e) {
            TableDataHighlighter highlighter = highlighterFactory.cellLabel(currentCol, 0, "Invalid!");
            throw new TableException(highlighter, e.getMessage(), e);
        }
    }


    private class ChooseNodeTypeState implements BuildingState {

        private final Pattern NODE_SPEC_PATTERN         = Pattern.compile("([^\\[\\]{}]*)(\\[])?(\\{.*})?");
        private final int     NODE_NAME_PATTERN_GROUP   = 1;
        private final int     IS_ARRAY_PATTERN_GROUP    = 2;
        private final int     NESTED_NODE_PATTERN_GROUP = 3;

        @Override
        public void handle(ObjectStructureNodes.CompositeNode parent, String nodeNameMustBeNull, String nodeSpec) {
            checkArgument(isNull(nodeNameMustBeNull),
                          "%s doesn't expect a node name ('%s'). This is a programming error.",
                          ChooseNodeTypeState.class.getSimpleName(),
                          nodeNameMustBeNull);

            Matcher matcher = NODE_SPEC_PATTERN.matcher(nodeSpec);
            checkArgument(matcher.matches(),
                          "Node specification \"%s\" doesn't match the expected pattern: %s",
                          nodeSpec,
                          NODE_SPEC_PATTERN.pattern());

            String nodeName = matcher.group(NODE_NAME_PATTERN_GROUP);
            String nestedNodeSpec = matcher.group(NESTED_NODE_PATTERN_GROUP);
            boolean isArrayNode = matchesArrayNode(matcher);

            if (isBlank(nodeName)) {
                //try guess the node name & type from previous node spec
                ObjectStructureNodes.CompositeNode child = takeChildNodeNameFromPreviousSimpleNode(parent);
                nodeName = parent.children.inverse().get(child);
                isArrayNode = (child instanceof ObjectStructureNodes.ArrayNode);
            }

            if (isArrayNode) {
                ADD_ARRAY_NODE.handle(parent, nodeName, nestedNodeSpec);

            } else if (!isBlank(nestedNodeSpec)) {
                ADD_COMPOSITE_NODE.handle(parent, nodeName, nestedNodeSpec);

            } else {
                ADD_SIMPLE_NODE.handle(parent, nodeName, null);
            }
        }

        private ObjectStructureNodes.CompositeNode takeChildNodeNameFromPreviousSimpleNode(ObjectStructureNodes.CompositeNode commonParent) {
            checkArgument(nonNull(ObjectStructureBuilder.this.previousSimpleNode),
                          "First node to be parsed ('%s') cannot use compressed format", currentNodeHeaderName);
            ObjectStructureNodes.Node child = ObjectStructureBuilder.this.previousSimpleNode;
            while (child.parent != commonParent) {
                child = child.parent;
                checkCommonParentPrecondition(nonNull(child), commonParent);
            }
            checkCommonParentPrecondition((child instanceof ObjectStructureNodes.CompositeNode), commonParent);
            return (ObjectStructureNodes.CompositeNode) child;
        }

        private void checkCommonParentPrecondition(boolean precondition, ObjectStructureNodes.CompositeNode commonParent) {
            checkArgument(precondition,
                          "Node to be parsed ('%s') and previous column node ('%s') do not have a common parent ('%s')",
                          currentNodeHeaderName,
                          previousSimpleNode.getFullName(),
                          commonParent.getFullName());
        }

        private boolean matchesArrayNode(Matcher matcher) {
            return nonNull(matcher.group(IS_ARRAY_PATTERN_GROUP));
        }
    }


    private class AddSimpleNodeState implements BuildingState {
        @Override
        public void handle(ObjectStructureNodes.CompositeNode parent, String nodeName, String nestedNodeSpec) {
            checkArgument(isNull(nestedNodeSpec),
                          "%s cannot handle nested node ('%s'). This is a programming error.",
                          AddSimpleNodeState.class.getSimpleName(),
                          nestedNodeSpec);

            checkArgument(!parent.children.containsKey(nodeName), "Duplicate use of node '%s'", nodeName);

            final ObjectStructureNodes.SimpleNode simpleNode = new ObjectStructureNodes.SimpleNode(parent, currentNodeHeaderName, currentCol);
            parent.children.put(nodeName, simpleNode);
            previousSimpleNode = simpleNode;
        }
    }


    private class AddCompositeNodeState implements BuildingState {

        private final Pattern RAW_NESTED_NODE_SPEC_PATTERN   = Pattern.compile("\\{(.+)}");
        private final int     NESTED_NODE_SPEC_PATTERN_GROUP = 1;

        @Override
        public void handle(ObjectStructureNodes.CompositeNode parent, String nodeName, String rawNestedNodeSpec) {
            final ObjectStructureNodes.CompositeNode compositeNode = getOrAttachCompositeNode(parent, nodeName);

            String nestedNodeSpec = unwrapNestedNodeSpec(rawNestedNodeSpec);

            CHOOSE_NODE_TYPE.handle(compositeNode, null, nestedNodeSpec);
        }

        private ObjectStructureNodes.CompositeNode getOrAttachCompositeNode(ObjectStructureNodes.CompositeNode parent, String nodeName) {
            ObjectStructureNodes.Node nested = parent.children.computeIfAbsent(nodeName,
                                                          key -> new ObjectStructureNodes.CompositeNode(parent));
            checkState(nested instanceof ObjectStructureNodes.CompositeNode,
                       "Composite node expected for '%s'", nodeName);
            return (ObjectStructureNodes.CompositeNode) nested;
        }

        private String unwrapNestedNodeSpec(String rawNestedNodeSpec) {
            Matcher matcher = RAW_NESTED_NODE_SPEC_PATTERN.matcher(rawNestedNodeSpec);
            checkArgument(matcher.matches(),
                          "Raw nested node specification '%s' doesn't match the expected pattern: %s",
                          rawNestedNodeSpec,
                          RAW_NESTED_NODE_SPEC_PATTERN.pattern());
            return matcher.group(NESTED_NODE_SPEC_PATTERN_GROUP);
        }
    }


    private class AddArrayNodeState implements BuildingState {

        @Override
        public void handle(ObjectStructureNodes.CompositeNode parent, String nodeName, String rawNestedNodeSpec) {
            final ObjectStructureNodes.CompositeNode arrayNode = getOrAttachArrayNode(parent, nodeName);

            if (isBlank(rawNestedNodeSpec)) {
                ADD_SIMPLE_NODE.handle(arrayNode, ObjectStructureNodes.ArrayNode.PRIMITIVE_CHILD_NAME, null);

            } else {
                ADD_COMPOSITE_NODE.handle(arrayNode, ObjectStructureNodes.ArrayNode.COMPOSITE_CHILD_NAME, rawNestedNodeSpec);
            }
        }

        private ObjectStructureNodes.ArrayNode getOrAttachArrayNode(ObjectStructureNodes.CompositeNode parent, String nodeName) {
            ObjectStructureNodes.Node nested = parent.children.computeIfAbsent(nodeName,
                                                          key -> new ObjectStructureNodes.ArrayNode(parent));
            checkState(nested instanceof ObjectStructureNodes.ArrayNode,
                       "Array node expected for '%s'", nodeName);
            return (ObjectStructureNodes.ArrayNode) nested;
        }
    }


    @FunctionalInterface
    private interface BuildingState {
        void handle(ObjectStructureNodes.CompositeNode parent, String nodeName, String nestedNodeSpec);
    }
}
