package net.lepshi.fitnesse.mutitables.plugin.tables;

import fitnesse.testsystems.slim.Table;
import net.lepshi.fitnesse.mutitables.plugin.TableException;
import net.lepshi.fitnesse.mutitables.plugin.common.CompositeTable;
import net.lepshi.fitnesse.mutitables.plugin.common.SubTable;
import net.lepshi.fitnesse.mutitables.plugin.common.TableDataHighlighterFactory;
import net.lepshi.fitnesse.mutitables.plugin.data.Rows;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static java.lang.String.format;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isNotBlank;


class HorizontalActionCombinationDiscovery {

    private final Table                       table;
    private final TableDataHighlighterFactory highlighterFactory;
    private final ActionCombinationFactory    actionCombinationFactory;
    private final String                      combinationTypeDisplayName;

    private final LinkedList<ActionSpec>  actionSpecs        = new LinkedList<>();
    private       Rows                    fixedRows;
    private final List<ActionCombination> actionCombinations = new ArrayList<>();


    HorizontalActionCombinationDiscovery(Table table, ActionCombinationFactory actionCombinationFactory) {
        this.table = table;
        this.highlighterFactory = new TableDataHighlighterFactory(table);
        this.actionCombinationFactory = actionCombinationFactory;
        this.combinationTypeDisplayName = actionCombinationFactory.getCombinationType()
                                                                  .getDisplayName();
    }


    List<ActionCombination> discoverActionCombinations() {
        checkHasAtLeastFourRows();
        initActionSpecs();
        initFixedRows();
        actionCombinations.clear();

        CombinationSpec currentCombinationSpec = new CombinationSpec(fixedRows.last + 1);
        for (int row = currentCombinationSpec.titleRow + 1; row < table.getRowCount(); row++) {
            if (isTitleRow(row)) {
                checkHasDataRows(currentCombinationSpec, row - 1);
                currentCombinationSpec.variableRows = Rows.range(currentCombinationSpec.titleRow + 1,
                                                                 row - 1);
                addActionCombination(currentCombinationSpec);
                currentCombinationSpec = new CombinationSpec(row);
            }
        }
        checkHasDataRows(currentCombinationSpec, table.getRowCount() - 1);
        currentCombinationSpec.variableRows = Rows.range(currentCombinationSpec.titleRow + 1,
                                                         table.getRowCount() - 1);
        addActionCombination(currentCombinationSpec);
        return actionCombinations;
    }

    private void initActionSpecs() {
        actionSpecs.clear();

        checkActionRow();

        actionSpecs.add(new ActionSpec(1));
        for (int col = 2; col < table.getColumnCountInRow(1); col++) {
            final String possibleActionType = table.getCellContents(col, 1);
            if (isNotBlank(possibleActionType)) {
                actionSpecs.getLast().lastCol = col - 1;
                actionSpecs.add(new ActionSpec(col));
            }
        }
        actionSpecs.getLast().lastCol = Integer.MAX_VALUE;
    }

    private void initFixedRows() {
        for (int row = 2; row < table.getRowCount(); row++) {
            if (isTitleRow(row)) {
                fixedRows = Rows.range(1, row - 1);
                return;
            }
        }
        throw new TableException(highlighterFactory.cellLabel(0, table.getRowCount() - 1,
                                                              "Any " + combinationTypeDisplayName + "?"),
                                 "No " + combinationTypeDisplayName + " found in column 0");
    }

    private void addActionCombination(CombinationSpec spec) {

        final Table titleTable = new SubTable(table,
                                              0, Integer.MAX_VALUE,
                                              spec.titleRow, spec.titleRow);

        final Table combinationReportingTable = new SubTable(table,
                                                             0, 0,
                                                             spec.variableRows.first, spec.variableRows.last);

        final List<Table> actionTables = actionSpecs.stream()
                                                    .map(actionSpec -> actionSpecToTable(actionSpec, spec))
                                                    .collect(toList());

        final ActionCombination actionCombination = actionCombinationFactory.createActionCombination(titleTable,
                                                                                                     combinationReportingTable,
                                                                                                     actionTables);
        actionCombinations.add(actionCombination);
    }

    private Table actionSpecToTable(ActionSpec actionSpec, CombinationSpec combinationSpec) {
        final int actionTypeRow = fixedRows.first;
        final Table actionTypeTable = new SubTable(table,
                                                   actionSpec.firstCol, actionSpec.firstCol,
                                                   actionTypeRow, actionTypeRow);

        final Table variableDataTable = new SubTable(table,
                                                     actionSpec.firstCol, actionSpec.lastCol,
                                                     combinationSpec.variableRows.first, combinationSpec.variableRows.last);

        final Rows fixedDataRows = fixedRows.exceptFirst();

        if (fixedDataRows.count() > 0) {
            final Table fixedDataTable = new SubTable(table,
                                                      actionSpec.firstCol, actionSpec.lastCol,
                                                      fixedDataRows.first, fixedDataRows.last);
            return new CompositeTable(actionTypeTable,
                                      fixedDataTable,
                                      variableDataTable);
        } else {
            return new CompositeTable(actionTypeTable,
                                      variableDataTable);
        }
    }

    private boolean isTitleRow(int row) {
        String cellContents = table.getCellContents(0, row);
        return isNotBlank(cellContents);
    }


    private void checkHasAtLeastFourRows() {
        if (table.getRowCount() < 4) {
            throw new TableException(highlighterFactory.cellLabel(0, 0, "At least 4 rows expected"),
                                     "At least 4 rows expected (tableType, actions, title, data)");
        }
    }

    private void checkHasDataRows(CombinationSpec combinationSpec, int lastCombinationRow) {
        if (lastCombinationRow <= combinationSpec.titleRow) {
            final String combinationTitle = table.getCellContents(0, combinationSpec.titleRow);
            throw new TableException(highlighterFactory.cellLabel(0, combinationSpec.titleRow, "Missing data rows"),
                                     format("%s '%s' must have at least 1 data row", combinationTypeDisplayName, combinationTitle));
        }
    }

    private void checkActionRow() {
        checkActionRowHasAtLeastTwoColumns();
        checkActionRowHasEmptyFirstColumn();
        checkActionRowHasProperFirstAction();
    }

    private void checkActionRowHasAtLeastTwoColumns() {
        if (table.getColumnCountInRow(1) < 2) {
            throw new TableException(highlighterFactory.cellLabel(0, 1, "At least 2 columns expected"),
                                     "Actions row must have at least 2 columns");
        }
    }

    private void checkActionRowHasEmptyFirstColumn() {
        if (isNotBlank(table.getCellContents(0, 1))) {
            throw new TableException(highlighterFactory.cellLabel(0, 1, "Cell must be empty"),
                                     "Column 0 in action type row must be empty");
        }
    }

    private void checkActionRowHasProperFirstAction() {
        final String firstActionType = table.getCellContents(1, 1);
        if (isBlank(firstActionType)) {
            throw new TableException(highlighterFactory.cellLabel(1, 1, "First action expected here"),
                                     "First action type expected in column 1");
        }
    }


    @RequiredArgsConstructor
    private static class ActionSpec {
        final int firstCol;
        int lastCol;
    }

    @RequiredArgsConstructor
    private static class CombinationSpec {
        final int titleRow;
        Rows variableRows;
    }
}
