package net.lepshi.fitnesse.mutitables.plugin.tables;

import fitnesse.testsystems.TestExecutionException;
import fitnesse.testsystems.slim.SlimTestContext;
import fitnesse.testsystems.slim.Table;
import fitnesse.testsystems.slim.tables.SlimAssertion;
import fitnesse.testsystems.slim.tables.SlimTable;
import fitnesse.testsystems.slim.tables.SlimTableFactory;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.lepshi.fitnesse.mutitables.plugin.TableException;
import net.lepshi.fitnesse.mutitables.plugin.common.TableDataHighlighterFactory;

import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Supplier;

import static fitnesse.testsystems.ExecutionResult.IGNORE;
import static java.lang.String.format;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;
import static net.lepshi.fitnesse.mutitables.plugin.PluginContext.pluginContext;

@Slf4j
class ActionCombination {

    @RequiredArgsConstructor(access = AccessLevel.PRIVATE)
    @Getter
    public enum CombinationType {
        TEST_CASE("tc", "TestCase", "Test Case"),
        BEFORE_EACH_TEST_STEP("before", "BeforeEach", "@BeforeEach test step"),
        AFTER_EACH_TEST_STEP("after", "AfterEach", "@AfterEach test step");

        private final String id;
        private final String annotationId;
        private final String displayName;

        public String decorateTitle(String title) {
            return format("%s('%s')", annotationId, title);
        }
    }

    @Getter
    private final CombinationType             combinationType;
    @Getter
    private final Supplier<String>            combinationIdSupplier;
    @Getter
    private final String                      combinationTitle;
    @Getter
    private final Table                       titleTable;
    @Getter
    private final List<Table>                 actionTables;
    private final AssertionsCustomizer        assertionsCustomizer;
    private final SlimTestContext             slimTestContext;
    private final List<Action>                actions;
    private final TableDataHighlighterFactory titleHighlighterFactory;
    private final Map<SlimAssertion, Action>  assertionToActionMap = new IdentityHashMap<>();

    private final SlimTableFactory slimTableFactory = pluginContext().getSlimTableFactory();


    ActionCombination(CombinationType combinationType,
                      Supplier<String> idSupplier,
                      Table titleTable,
                      List<Table> actionTables,
                      AssertionsCustomizer assertionsCustomizer,
                      SlimTestContext slimTestContext) {

        this.combinationType = combinationType;
        this.combinationIdSupplier = idSupplier;
        this.combinationTitle = titleTable.getCellContents(0, 0);
        this.titleTable = titleTable;
        this.actionTables = actionTables;
        this.assertionsCustomizer = assertionsCustomizer;
        this.slimTestContext = slimTestContext;
        this.titleHighlighterFactory = new TableDataHighlighterFactory(titleTable);

        final AtomicInteger actionIdx = new AtomicInteger(0);
        this.actions = actionTables.stream()
                                   .map(actionTable -> new Action(actionIdx.getAndIncrement(), actionTable))
                                   .collect(toList());
    }


    List<SlimAssertion> getAssertions() {
        try {
            List<SlimAssertion> assertions = actions.stream()
                                                    .map(Action::getSlimAssertions)
                                                    .flatMap(List::stream)
                                                    .collect(toList());

            return assertionsCustomizer.customize(assertions);

        } catch (RuntimeException e) {
            slimTestContext.incrementErroredTestsCount();
            final String errorMsg = format("Failed to get assertions for %s [%s]: %s",
                                           combinationType.decorateTitle(combinationTitle),
                                           combinationIdSupplier.get(),
                                           e.getMessage());
            LOG.error(errorMsg, e);
            titleHighlighterFactory.table(IGNORE, errorMsg).highlight();
            if (e instanceof TableException) {
                ((TableException) e).highlightTableData();
            }
            return emptyList();
        }
    }

    String actionIdOf(SlimAssertion assertion) {
        final Action action = assertionToActionMap.get(assertion);
        return String.valueOf(action.actionIdx);
    }


    private class Action {
        private int                         actionIdx;
        final   Table                       actionTable;
        final   TableDataHighlighterFactory actionHighlighterFactory;

        Action(int idx, Table actionTable) {
            this.actionIdx = idx;
            this.actionTable = actionTable;
            this.actionHighlighterFactory = new TableDataHighlighterFactory(actionTable);
        }

        List<SlimAssertion> getSlimAssertions() {
            final String combinationId = ActionCombination.this.combinationIdSupplier.get();
            final String actionId = format("action[%d]@%s", actionIdx, combinationId);
            try {
                SlimTable slimTable = slimTableFactory.makeSlimTable(actionTable,
                                                                     actionId,
                                                                     slimTestContext);
                List<SlimAssertion> assertions = slimTable.getAssertions();
                mapAssertionsToAction(assertions);
                return assertions;

            } catch (TestExecutionException e) {
                throw new RuntimeException(format("Action error [%s]: %s", actionId, e.getMessage()));
            }
        }

        private void mapAssertionsToAction(List<SlimAssertion> actionAssertions) {
            actionAssertions.forEach(
                    actionAssertion -> assertionToActionMap.put(actionAssertion, Action.this));
        }
    }
}
