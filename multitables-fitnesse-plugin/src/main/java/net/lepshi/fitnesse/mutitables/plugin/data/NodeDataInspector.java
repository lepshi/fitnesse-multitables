package net.lepshi.fitnesse.mutitables.plugin.data;

import fitnesse.testsystems.ExecutionResult;
import fitnesse.testsystems.slim.Table;
import net.lepshi.fitnesse.mutitables.plugin.common.TableDataHighlighterFactory;
import net.lepshi.fitnesse.mutitables.plugin.TableDataHighlighter;
import net.lepshi.fitnesse.mutitables.plugin.TableException;
import lombok.RequiredArgsConstructor;

import java.util.function.Predicate;

import static java.lang.String.format;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

class NodeDataInspector {

    private final Table                       data;
    private final ObjectStructureNodes.Node   node;
    private final TableDataHighlighterFactory highlighterFactory;

    NodeDataInspector(Table data, ObjectStructureNodes.Node node) {
        this.data = data;
        this.node = node;
        this.highlighterFactory = new TableDataHighlighterFactory(data);
    }


    boolean hasData(Rows rows) {
        return hasData(allChildNodes(), rows);
    }

    boolean hasData(Predicate<ObjectStructureNodes.Node> includeChildFilter, Rows rows) {
        return node.handleBy(new HasDataGetter(includeChildFilter, rows));
    }

    void checkHasNoData(Predicate<ObjectStructureNodes.Node> includeChildFilter, Rows rows, String errorPrefix) {
        node.handleBy(new NoDataChecker(includeChildFilter, rows, errorPrefix));
    }


    private class HasDataGetter extends DFSearchingNodeInspector {

        private final Rows rows;

        HasDataGetter(Predicate<ObjectStructureNodes.Node> includeChildFilter, Rows rows) {
            super(includeChildFilter);
            this.rows = rows;
        }

        @Override
        public Boolean handle(ObjectStructureNodes.SimpleNode simpleNode) {
            return rows.stream()
                       .anyMatch(row -> isNotBlank(data.getCellContents(simpleNode.col, row)));
        }
    }


    private class NoDataChecker extends DFSearchingNodeInspector {

        private final Rows   rows;
        private final String errorPrefix;

        NoDataChecker(Predicate<ObjectStructureNodes.Node> includeChildFilter, Rows rows, String errorPrefix) {
            super(includeChildFilter);
            this.rows = rows;
            this.errorPrefix = errorPrefix;
        }


        @Override
        public Boolean handle(ObjectStructureNodes.SimpleNode simpleNode) {
            rows.stream()
                .forEach(row -> {
                    final String cellData = data.getCellContents(simpleNode.col, row);
                    if (isNotBlank(cellData)) {
                        TableDataHighlighter cellDataHighlighter = highlighterFactory.cell(ExecutionResult.FAIL,
                                                                                           simpleNode.col,
                                                                                           row,
                                                                                           "expected empty!");
                        throw new TableException(cellDataHighlighter, format("%s: Expected empty, but was node('%s')=value('%s')",
                                                                             errorPrefix,
                                                                             simpleNode.getFullName(),
                                                                             cellData));
                    }
                });

            return Boolean.FALSE; //force to visit all nodes
        }

    }


    @RequiredArgsConstructor
    private abstract class DFSearchingNodeInspector implements ObjectStructureNodes.NodeHandler<Boolean> {

        private final Predicate<ObjectStructureNodes.Node> includeChildFilter;

        @Override
        public Boolean handle(ObjectStructureNodes.CompositeNode compositeNode) {
            return compositeNode.children.values()
                                         .stream()
                                         .filter(includeChildFilter)
                                         .anyMatch(node -> node.handleBy(this));
        }

        @Override
        public Boolean handle(ObjectStructureNodes.ArrayNode arrayNode) {
            return handle((ObjectStructureNodes.CompositeNode) arrayNode);
        }
    }


    static Predicate<ObjectStructureNodes.Node> nonArrayNodes() {
        return node -> !(node instanceof ObjectStructureNodes.ArrayNode);
    }

    private static Predicate<ObjectStructureNodes.Node> allChildNodes() {
        return node -> true;
    }
}
