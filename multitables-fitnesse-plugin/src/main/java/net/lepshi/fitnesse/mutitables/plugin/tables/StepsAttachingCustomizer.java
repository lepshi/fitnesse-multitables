package net.lepshi.fitnesse.mutitables.plugin.tables;

import fitnesse.testsystems.ExecutionResult;
import fitnesse.testsystems.slim.Table;
import fitnesse.testsystems.slim.tables.SlimAssertion;
import net.lepshi.fitnesse.mutitables.plugin.common.TableDataHighlighterFactory;
import net.lepshi.fitnesse.mutitables.plugin.tables.ObservedSlimAssertion.AssertionObserver;

import java.util.ArrayList;
import java.util.List;

import static com.google.common.collect.Iterables.concat;
import static com.google.common.collect.Lists.newArrayList;
import static java.lang.String.format;
import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toCollection;
import static net.lepshi.fitnesse.mutitables.plugin.tables.ObservedSlimAssertion.observedAssertion;


class StepsAttachingCustomizer implements AssertionsCustomizer {

    private final EachTestSteps               steps;
    private final TableDataHighlighterFactory highlighterFactory;
    private final int                         beforeStepsReportingRow;
    private final int                         afterStepsReportingRow;


    StepsAttachingCustomizer(EachTestSteps steps, Table stepsReportingTable) {
        this.steps = steps;
        this.highlighterFactory = new TableDataHighlighterFactory(stepsReportingTable);
        this.beforeStepsReportingRow = 0;
        this.afterStepsReportingRow = stepsReportingTable.getRowCount() - 1;
    }


    @Override
    public List<SlimAssertion> customize(List<SlimAssertion> assertions) {
        return newArrayList(concat(assertionsFrom(steps.getBeforeSteps(), beforeStepsReportingRow),
                                   assertions,
                                   assertionsFrom(steps.getAfterSteps(), afterStepsReportingRow)));
    }

    private List<SlimAssertion> assertionsFrom(List<ActionCombination> steps, int reportingRow) {
        final List<SlimAssertion> assertions = new ArrayList<>();
        for (ActionCombination step : steps) {
            step.getAssertions()
                .stream()
                .map(assertion -> observedAssertion(assertion, resultReportingObserver(step,
                                                                                       assertion,
                                                                                       reportingRow)))
                .collect(toCollection(() -> assertions));
        }
        return assertions;
    }


    private AssertionObserver resultReportingObserver(ActionCombination step,
                                                      SlimAssertion assertion,
                                                      int reportingRow) {
        return stepResult -> {
            if (asList(ExecutionResult.FAIL, ExecutionResult.ERROR)
                    .contains(stepResult.getExecutionResult())) {

                highlighterFactory.row(null,
                                       reportingRow,
                                       format("%s[%s][%s]: %s",
                                              step.getCombinationType()
                                                  .decorateTitle(step.getCombinationTitle()),
                                              step.actionIdOf(assertion),
                                              stepResult.getExecutionResult().name(),
                                              stepResult.getMessage()))
                                  .highlight();
            }
        };
    }
}
