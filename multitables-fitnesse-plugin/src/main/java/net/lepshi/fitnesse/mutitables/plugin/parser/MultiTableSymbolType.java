package net.lepshi.fitnesse.mutitables.plugin.parser;

import fitnesse.wikitext.parser.Matcher;
import fitnesse.wikitext.parser.Maybe;
import fitnesse.wikitext.parser.Parser;
import fitnesse.wikitext.parser.Rule;
import fitnesse.wikitext.parser.Symbol;
import fitnesse.wikitext.parser.SymbolType;
import fitnesse.wikitext.parser.Table;
import fitnesse.wikitext.parser.Translation;
import fitnesse.wikitext.parser.Translator;
import lombok.extern.slf4j.Slf4j;


@Slf4j
public class MultiTableSymbolType extends SymbolType implements Rule, Translation {

    private final Table baseSymbol = Table.symbolType;

    public MultiTableSymbolType() {
        super("MultiTable");
        wikiMatcher(new Matcher().startLine().string("mt|"));
        wikiRule(this);
        htmlTranslation(this);
    }


    @Override
    public Maybe<Symbol> parse(Symbol current, Parser parser) {
        final String originalContent = current.getContent();
        current.setContent(originalContent.replace("mt|", "!|"));
        return baseSymbol.parse(current, parser);
    }


    @Override
    public String toTarget(Translator translator, Symbol symbol) {
        return baseSymbol.toTarget(translator, symbol);
    }
}
