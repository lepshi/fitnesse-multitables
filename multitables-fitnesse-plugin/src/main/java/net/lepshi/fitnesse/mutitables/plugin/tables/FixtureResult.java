package net.lepshi.fitnesse.mutitables.plugin.tables;

import fitnesse.testsystems.ExecutionResult;
import fitnesse.testsystems.TestResult;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;

import java.util.Collections;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.google.common.base.Preconditions.checkArgument;
import static java.util.Objects.nonNull;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
class FixtureResult {

    private static final Pattern CODE_AND_MESSAGE_PATTERN = Pattern.compile("([^:]*)(:(.*))?");

    final String code;
    final String message;


    static FixtureResult parseFrom(String codeAndMessage) {

        final Matcher matcher = CODE_AND_MESSAGE_PATTERN.matcher(codeAndMessage);
        checkArgument(matcher.matches(), "String doesn't match expected pattern: %s", codeAndMessage);

        String code = matcher.group(1);
        String message = (matcher.groupCount() >= 3) ? matcher.group(3)
                                                     : null;
        return new FixtureResult(code, message);
    }


    ExecutionResult asExecutionResult() {
        switch (code) {
            case "pass":
                return ExecutionResult.PASS;
            case "fail":
                return ExecutionResult.FAIL;
            case "ignore":
                return ExecutionResult.IGNORE;
            case "plain":
            case "":
            default:
                return null;
        }
    }

    TestResult asTestResult() {
        return this.new FixtureTestResult();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (nonNull(code)) {
            sb.append(code);
        }
        if (nonNull(message)) {
            sb.append(":");
            sb.append(message);
        }
        return sb.toString();
    }


    @RequiredArgsConstructor
    private class FixtureTestResult implements TestResult {

        @Override
        public boolean doesCount() {
            return false;
        }

        @Override
        public boolean hasActual() {
            return false;
        }

        @Override
        public String getActual() {
            return null;
        }

        @Override
        public boolean hasExpected() {
            return false;
        }

        @Override
        public String getExpected() {
            return null;
        }

        @Override
        public boolean hasMessage() {
            return nonNull(message);
        }

        @Override
        public String getMessage() {
            return message;
        }

        @Override
        public ExecutionResult getExecutionResult() {
            return asExecutionResult();
        }

        @Override
        public Map<String, ?> getVariablesToStore() {
            return Collections.emptyMap();
        }
    }

}
