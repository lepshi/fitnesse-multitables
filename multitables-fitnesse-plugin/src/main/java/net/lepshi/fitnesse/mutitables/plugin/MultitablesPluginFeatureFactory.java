package net.lepshi.fitnesse.mutitables.plugin;

import fitnesse.plugins.PluginFeatureFactoryBase;
import fitnesse.testsystems.slim.tables.SlimTableFactory;
import fitnesse.wikitext.parser.SymbolProvider;
import net.lepshi.fitnesse.mutitables.plugin.parser.MultiTableSymbolType;
import net.lepshi.fitnesse.mutitables.plugin.tables.MultiTable;
import net.lepshi.fitnesse.mutitables.plugin.tables.TabularJsonTable;
import lombok.extern.slf4j.Slf4j;

import static net.lepshi.fitnesse.mutitables.plugin.PluginContext.pluginContext;

@Slf4j
public class MultitablesPluginFeatureFactory extends PluginFeatureFactoryBase {

    public MultitablesPluginFeatureFactory() {
        LOG.info("Init MultiTables Plugin");
    }


    @Override
    public void registerSymbolTypes(SymbolProvider symbolProvider) {
        symbolProvider.add(new MultiTableSymbolType());
    }

    @Override
    public void registerSlimTables(SlimTableFactory slimTableFactory) {
        pluginContext().setSlimTableFactory(slimTableFactory);
        slimTableFactory.addTableType("multiTable", MultiTable.class);
        slimTableFactory.addTableType("tabularJson", TabularJsonTable.class);
    }

}
