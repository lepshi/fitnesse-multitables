package net.lepshi.fitnesse.mutitables.plugin.tables;

import fitnesse.testsystems.slim.Table;
import net.lepshi.fitnesse.mutitables.plugin.tables.ActionCombination.CombinationType;

import java.util.List;

interface ActionCombinationFactory {

    CombinationType getCombinationType();

    ActionCombination createActionCombination(Table titleTable,
                                              Table combinationReportingTable,
                                              List<Table> actionTables);
}
