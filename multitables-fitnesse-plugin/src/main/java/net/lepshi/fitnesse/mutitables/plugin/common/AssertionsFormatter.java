package net.lepshi.fitnesse.mutitables.plugin.common;

import fitnesse.testsystems.Assertion;

import java.util.Collection;

import static org.apache.commons.lang3.StringUtils.stripEnd;


public class AssertionsFormatter {

    public static String formatAssertions(String header, Collection<? extends Assertion> assertions) {
        final StringBuilder builder = new StringBuilder(header);
        int idx = 0;
        for (Assertion assertion : assertions) {
            builder.append("\n[")
                   .append(idx)
                   .append("]\n")
                   .append(stripEnd(assertion.toString(), "\r\n"));
            idx++;
        }
        return builder.toString();
    }

}
