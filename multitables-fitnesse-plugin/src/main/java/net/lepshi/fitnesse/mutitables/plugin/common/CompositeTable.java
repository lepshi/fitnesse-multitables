package net.lepshi.fitnesse.mutitables.plugin.common;

import fitnesse.testsystems.slim.Table;
import fitnesse.testsystems.slim.results.SlimExceptionResult;
import fitnesse.testsystems.slim.results.SlimTestResult;

import java.util.ArrayList;
import java.util.List;

import static com.google.common.base.Preconditions.checkArgument;
import static java.util.Collections.unmodifiableList;


public class CompositeTable extends UnsupportedOperationsTable {

    private final List<TableComponent> tableComponents;

    public CompositeTable(Table... tables) {
        checkArgument(tables.length > 0, "No tableComponents given");
        this.tableComponents = unmodifiableList(buildTableComponents(tables));
    }

    private static List<TableComponent> buildTableComponents(Table[] tables) {
        final List<TableComponent> tableComponents = new ArrayList<>(tables.length);
        int nextCompositeRow = 0;
        for (Table table : tables) {
            TableComponent tableComponent = new TableComponent(table, nextCompositeRow);
            tableComponents.add(tableComponent);
            nextCompositeRow = tableComponent.lastCompositeRow + 1;
        }
        return tableComponents;
    }


    @Override
    public boolean isTearDown() {
        return tableComponents.get(0).table.isTearDown();
    }

    @Override
    public String getCellContents(int col, int row) {
        TableComponent component = componentForRow(row);
        int componentRow = component.getComponentRow(row);
        return component.table.getCellContents(col, componentRow);
    }

    @Override
    public int getRowCount() {
        TableComponent lastComponent = tableComponents.get(tableComponents.size() - 1);
        return lastComponent.lastCompositeRow + 1;
    }

    @Override
    public int getColumnCountInRow(int row) {
        TableComponent component = componentForRow(row);
        int componentRow = component.getComponentRow(row);
        return component.table.getColumnCountInRow(componentRow);
    }

    @Override
    public void substitute(int col, int row, String content) {
        TableComponent component = componentForRow(row);
        int componentRow = component.getComponentRow(row);
        component.table.substitute(col, componentRow, content);
    }

    @Override
    public void updateContent(int col, int row, SlimTestResult testResult) {
        TableComponent component = componentForRow(row);
        int componentRow = component.getComponentRow(row);
        component.table.updateContent(col, componentRow, testResult);
    }

    @Override
    public void updateContent(int col, int row, SlimExceptionResult exceptionResult) {
        TableComponent component = componentForRow(row);
        int componentRow = component.getComponentRow(row);
        component.table.updateContent(col, componentRow, exceptionResult);
    }


    private TableComponent componentForRow(int compositeRow) {
        for (TableComponent component : tableComponents) {
            if (compositeRow <= component.lastCompositeRow) {
                return component;
            }
        }
        throw new IllegalArgumentException("No such row: " + compositeRow);
    }


    private static class TableComponent {
        private final Table table;
        private final int firstCompositeRow;
        private final int lastCompositeRow;

        TableComponent(Table table, int firstCompositeRow) {
            this.table = table;
            this.firstCompositeRow = firstCompositeRow;
            this.lastCompositeRow = firstCompositeRow + table.getRowCount() - 1;
        }

        int getComponentRow(int compositeRow) {
            return compositeRow - firstCompositeRow;
        }
    }
}
