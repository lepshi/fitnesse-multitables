package net.lepshi.fitnesse.mutitables.plugin.tables;

import fitnesse.testsystems.TestResult;
import fitnesse.testsystems.slim.results.SlimTestResult;
import fitnesse.testsystems.slim.tables.SlimAssertion;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

import static com.google.common.base.Preconditions.checkState;
import static java.util.Objects.isNull;
import static java.util.stream.Collectors.toList;
import static net.lepshi.fitnesse.mutitables.plugin.tables.ObservedSlimAssertion.observedAssertion;


@Slf4j
@RequiredArgsConstructor
class AllExpectationsEvaluatedNotifyingCustomizer implements AssertionsCustomizer {

    private final AllExpectationsEvaluatedCallback allExpectationsEvaluatedCallback;


    @Override
    public List<SlimAssertion> customize(List<SlimAssertion> assertions) {
        final AssertionsCombination combination = new AssertionsCombination(assertions.size());
        return assertions.stream()
                         .map(assertion -> observedAssertion(assertion, combination::addTestResult))
                         .collect(toList());
    }


    @RequiredArgsConstructor
    private class AssertionsCombination {
        private final int              assertionsCount;
        private final List<TestResult> collectedTestResults = new ArrayList<>();

        void addTestResult(TestResult testResult) {
            checkState(collectedTestResults.size() < assertionsCount);
            collectedTestResults.add(fixTestResult(testResult));
            if (collectedTestResults.size() == assertionsCount) {
                notifyCallbackSafely();
            }
        }

        private TestResult fixTestResult(TestResult testResult) {
            if (isNull(testResult)) {
                return SlimTestResult.plain();
            }
            return testResult;
        }

        private void notifyCallbackSafely() {
            try {
                allExpectationsEvaluatedCallback.notifyAllExpectationsEvaluated(collectedTestResults);
            } catch (RuntimeException e) {
                LOG.error("Failed to notify about 'all expectations evaluated' event", e);
            }
        }
    }


    @FunctionalInterface
    public interface AllExpectationsEvaluatedCallback {
        void notifyAllExpectationsEvaluated(List<TestResult> testResults);
    }
}
