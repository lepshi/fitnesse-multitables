package net.lepshi.fitnesse.mutitables.plugin.tables;

import fitnesse.testsystems.slim.SlimTestContext;
import fitnesse.testsystems.slim.Table;
import fitnesse.testsystems.slim.results.SlimTestResult;
import fitnesse.testsystems.slim.tables.SlimAssertion;
import fitnesse.testsystems.slim.tables.SlimTable;
import net.lepshi.fitnesse.mutitables.plugin.TableException;
import net.lepshi.fitnesse.mutitables.plugin.common.TableDataHighlighterFactory;
import lombok.extern.slf4j.Slf4j;
import net.lepshi.fitnesse.mutitables.plugin.common.AssertionsFormatter;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Stream;

import static com.google.common.collect.Lists.transform;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;


@Slf4j
public class MultiTable extends SlimTable {

    private static final AtomicReference<EachTestSteps> EACH_TEST_ACCUMULATED_STEPS = new AtomicReference<>(new EachTestSteps());


    private final TableDataHighlighterFactory highlighterFactory;

    public MultiTable(Table table, String id, SlimTestContext testContext) {
        super(table, id, testContext);
        this.highlighterFactory = new TableDataHighlighterFactory(table);
    }


    @Override
    protected String getTableType() {
        return "multiTable";
    }

    @Override
    public List<SlimAssertion> getAssertions() {
        try {
            final ActionCombination.CombinationType combinationType = chooseCombinationType();
            switch (combinationType) {
                case TEST_CASE:
                    final EachTestSteps lastAccumulatedSteps = EACH_TEST_ACCUMULATED_STEPS.getAndSet(new EachTestSteps());
                    final TestCaseFactory testCaseFactory = new TestCaseFactory(lastAccumulatedSteps, getTestContext());
                    final List<ActionCombination> testCases = discoverHorizontalUsing(testCaseFactory);
                    logStartFetchingAssertions(combinationType, testCases);
                    final List<SlimAssertion> testCaseActions = testCases.stream()
                                                                         .map(this::getTestCaseAssertions)
                                                                         .flatMap(List::stream)
                                                                         .collect(toList());
                    table.updateContent(0, 0, SlimTestResult.pass());
                    return testCaseActions;

                case BEFORE_EACH_TEST_STEP:
                case AFTER_EACH_TEST_STEP:
                    final EachTestStepFactory stepFactory = new EachTestStepFactory(combinationType, getTestContext());
                    final List<ActionCombination> eachTestSteps = discoverHorizontalUsing(stepFactory);
                    logStartFetchingAssertions(combinationType, eachTestSteps);
                    EACH_TEST_ACCUMULATED_STEPS.get().addSteps(combinationType, eachTestSteps);
                    table.updateContent(0, 0, SlimTestResult.pass());
                    return emptyList();

                default:
                    throw new IllegalArgumentException("Unknown type of action combinations in multi-table: " + combinationType);
            }

        } catch (RuntimeException e) {
            getTestContext().incrementErroredTestsCount();
            LOG.debug("MultiTable error: {}", e.getMessage(), e);
            if (e instanceof TableException) {
                ((TableException) e).highlightTableData();
            }
            highlighterFactory.table(TableDataHighlighterFactory.NONE_EXEC_RESULT, "MultiTable error: " + e.getMessage())
                              .highlight();
            return emptyList();
        }
    }

    private ActionCombination.CombinationType chooseCombinationType() {
        return Stream.of(ActionCombination.CombinationType.values())
                     .filter(this::matchesFixtureType)
                     .findAny()
                     .orElse(ActionCombination.CombinationType.TEST_CASE);
    }

    private boolean matchesFixtureType(ActionCombination.CombinationType type) {
        return getFixtureName().contains(type.getAnnotationId());
    }


    private List<ActionCombination> discoverHorizontalUsing(ActionCombinationFactory factory) {
        return new HorizontalActionCombinationDiscovery(table, factory).discoverActionCombinations();
    }

    private void logStartFetchingAssertions(ActionCombination.CombinationType combinationType, List<ActionCombination> actionCombinations) {
        LOG.debug("Fetching assertions for {} {}(s): {}",
                  actionCombinations.size(),
                  combinationType.getDisplayName(),
                  transform(actionCombinations, ActionCombination::getCombinationTitle));
    }

    private List<SlimAssertion> getTestCaseAssertions(ActionCombination actionCombination) {
        final List<SlimAssertion> assertions = actionCombination.getAssertions();
        return logFetchedAssertions(actionCombination, assertions);
    }

    private List<SlimAssertion> logFetchedAssertions(ActionCombination actionCombination, List<SlimAssertion> assertions) {
        if (LOG.isTraceEnabled()) {
            LOG.trace("Fetched {} assertion(s) for test case '{}': {}",
                      assertions.size(),
                      actionCombination.getCombinationTitle(),
                      AssertionsFormatter.formatAssertions("", assertions));
        }
        return assertions;
    }

}
