package net.lepshi.fitnesse.mutitables.plugin.tables;

import fitnesse.testsystems.slim.SlimTestContext;
import fitnesse.testsystems.slim.Table;
import fitnesse.testsystems.slim.tables.SlimAssertion;
import net.lepshi.fitnesse.mutitables.plugin.common.ReadOnlyTable;
import net.lepshi.fitnesse.mutitables.plugin.tables.ActionCombination.CombinationType;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static java.lang.String.format;
import static java.util.stream.Collectors.toList;

@Slf4j
@RequiredArgsConstructor
class EachTestStepFactory implements ActionCombinationFactory {

    private final CombinationType combinationType;
    private final SlimTestContext testContext;
    private final AtomicInteger   nextStepIdx = new AtomicInteger(0);

    @Override
    public CombinationType getCombinationType() {
        return combinationType;
    }

    @Override
    public ActionCombination createActionCombination(Table titleTable,
                                                     Table combinationReportingTable,
                                                     List<Table> actionTables) {

        final String stepId = combinationType.getId() + nextStepIdx.getAndIncrement();
        final AtomicInteger currentCloneIdx = new AtomicInteger(0);

        final List<Table> readOnlyActionTables = actionTables.stream()
                                                             .map(ReadOnlyTable::readOnly)
                                                             .collect(toList());

        return new ActionCombination(getCombinationType(),
                                     () -> nextStepIdClone(stepId, currentCloneIdx.get()),
                                     titleTable,
                                     readOnlyActionTables,
                                     assertions -> assertions,
                                     testContext) {

            @Override
            List<SlimAssertion> getAssertions() {
                try {
                    return super.getAssertions();
                } finally {
                    currentCloneIdx.incrementAndGet();
                }
            }
        };
    }

    private String nextStepIdClone(String stepId, int cloneIdx) {
        return format("%s[clone%d]", stepId, cloneIdx);
    }

}
