package net.lepshi.fitnesse.mutitables.plugin.data;

import com.google.gson.stream.JsonWriter;
import fitnesse.testsystems.ExecutionResult;
import fitnesse.testsystems.slim.Table;
import net.lepshi.fitnesse.mutitables.plugin.common.TableDataHighlighterFactory;
import net.lepshi.fitnesse.mutitables.plugin.TableDataHighlighter;
import net.lepshi.fitnesse.mutitables.plugin.TableException;
import net.lepshi.fitnesse.mutitables.plugin.data.ObjectStructureNodes.*;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.util.Map;
import java.util.Optional;
import java.util.Stack;
import java.util.concurrent.Callable;

import static com.google.common.base.Preconditions.checkState;
import static java.lang.String.format;
import static java.util.Objects.nonNull;
import static net.lepshi.fitnesse.mutitables.plugin.data.NodeDataInspector.nonArrayNodes;
import static net.lepshi.fitnesse.mutitables.plugin.data.ObjectStructureNodes.ArrayNode.PRIMITIVE_CHILD_NAME;
import static net.lepshi.fitnesse.mutitables.plugin.data.Rows.range;
import static net.lepshi.fitnesse.mutitables.plugin.data.Rows.single;

class TableAsJsonWriter implements NodeHandler<Void> {

    private final Table                       data;
    private final TableDataHighlighterFactory highlighterFactory;
    private final JsonWriter                  jsonWriter;
    private final Stack<Rows>                 rowsStack = new Stack<>();
    private       Rows                        rows;

    TableAsJsonWriter(Table data, JsonWriter jsonWriter) {
        this.data = data;
        this.jsonWriter = jsonWriter;
        this.highlighterFactory = new TableDataHighlighterFactory(data);
    }

    void writeAsJson(Node rootNode) {
        this.rows = range(1, data.getRowCount() - 1);
        rootNode.handleBy(this);
    }


    private <T> T performUnchecked(Callable<T> unsafeOperation) {
        try {
            return unsafeOperation.call();
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Void handle(SimpleNode simpleNode) {
        return performUnchecked(() -> {
            checkNoDataOnOtherThanBaseRow(simpleNode, "Unexpected data on other than base row");

            jsonWriter.value(data.getCellContents(simpleNode.col, rows.first));

            return null;
        });
    }

    @Override
    public Void handle(CompositeNode compositeNode) {
        return performUnchecked(() -> {
            jsonWriter.beginObject();

            if (new NodeDataInspector(data, compositeNode).hasData(single(rows.first))) {
                for (Map.Entry<String, Node> entry : compositeNode.children.entrySet()) {
                    final String name = entry.getKey();
                    final Node node = entry.getValue();
                    jsonWriter.name(name);

                    node.handleBy(this);
                }
            }

            jsonWriter.endObject();

            return null;
        });
    }

    @Override
    public Void handle(ArrayNode arrayNode) {
        return performUnchecked(() -> {
            jsonWriter.beginArray();

            final NodeDataInspector nodeInspector = new NodeDataInspector(data, arrayNode);

            final PrimitiveElementsNoGapsChecker noGapsChecker = new PrimitiveElementsNoGapsChecker(arrayNode.children);

            ArrayElemRange currentArrayElemRange = new InitialDummyArrayElemRange();

            boolean hasDataOnFirstRow = nodeInspector.hasData(nonArrayNodes(), single(rows.first));
            if (!hasDataOnFirstRow) {
                checkNoDataOnOtherThanBaseRow(arrayNode, "Unexpected data on other than base row (while base row is empty)");
            }

            boolean isFirstElem = true;
            for (int row = rows.first; row <= rows.last; row++) {
                if (nodeInspector.hasData(nonArrayNodes(), single(row)) ||
                        (nodeInspector.hasData(single(row)) && row == rows.first)) {

                    if (!isFirstElem) {
                        noGapsChecker.checkNoGaps(currentArrayElemRange, row);
                    }
                    currentArrayElemRange.writeArrayElemData();
                    currentArrayElemRange = new ArrayElemRangeImpl(arrayNode.children, row);
                    isFirstElem = false;
                }
                currentArrayElemRange.setElemLastRow(row);
            }
            currentArrayElemRange.writeArrayElemData();

            jsonWriter.endArray();

            return null;
        });
    }

    private void checkNoDataOnOtherThanBaseRow(Node baseNode, String errorPrefix) {
        new NodeDataInspector(data, baseNode).checkHasNoData(nonArrayNodes(),
                                                             rows.exceptFirst(),
                                                             errorPrefix);
    }


    private class PrimitiveElementsNoGapsChecker {

        private final SimpleNode primitiveArrayElementNode;

        private PrimitiveElementsNoGapsChecker(Map<String, Node> elementNodes) {
            this.primitiveArrayElementNode = (SimpleNode) elementNodes.get(PRIMITIVE_CHILD_NAME);
            if (nonNull(primitiveArrayElementNode)) {
                checkState(elementNodes.size() == 1, "Array node's primitive child is not a single child");
            }
        }

        private void checkNoGaps(ArrayElemRange currentArrayElemRange, int nextElemRow) {
            Optional.ofNullable(primitiveArrayElementNode)
                    .ifPresent(node -> {
                        if (currentArrayElemRange.rows().count() > 1) {
                            TableDataHighlighter cellDataHighlighter = highlighterFactory.cell(ExecutionResult.FAIL,
                                                                                               node.col,
                                                                                               currentArrayElemRange.rows().first + 1,
                                                                                               "no gaps!");
                            String nextElemValue = data.getCellContents(node.col, nextElemRow);
                            throw new TableException(cellDataHighlighter, format(
                                    "Node('%s'): No gap is allowed before simple (non-composite) array value '%s'",
                                    node.getFullName(),
                                    nextElemValue));
                        }
                    });
        }
    }


    private interface ArrayElemRange {
        void setElemLastRow(int row);

        Rows rows();

        void writeArrayElemData();
    }

    private class InitialDummyArrayElemRange implements ArrayElemRange {
        @Override
        public void setElemLastRow(int row) {
            //
        }

        @Override
        public Rows rows() {
            return null;
        }

        @Override
        public void writeArrayElemData() {
            //
        }
    }

    @RequiredArgsConstructor
    private class ArrayElemRangeImpl implements ArrayElemRange {
        private final Map<String, Node> elementNodes;
        private final int               elemFirstRow;
        @Setter
        private       int               elemLastRow;

        @Override
        public Rows rows() {
            return Rows.range(elemFirstRow, elemLastRow);
        }

        @Override
        public void writeArrayElemData() {
            for (Node node : elementNodes.values()) {
                rowsStack.push(rows);
                rows = range(elemFirstRow, elemLastRow);
                node.handleBy(TableAsJsonWriter.this);
                rows = rowsStack.pop();
            }
        }
    }

}
