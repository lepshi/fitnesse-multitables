package net.lepshi.fitnesse.mutitables.plugin.common;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
@ToString
public class Cell {
    public final int col;
    public final int row;

    public static Cell cell(int col, int row) {
        return new Cell(col, row);
    }
}
