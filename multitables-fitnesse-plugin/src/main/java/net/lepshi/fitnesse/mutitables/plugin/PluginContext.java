package net.lepshi.fitnesse.mutitables.plugin;

import fitnesse.testsystems.slim.tables.SlimTableFactory;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter(value = AccessLevel.PACKAGE)
public class PluginContext {

    private static final PluginContext SINGLETON = new PluginContext();

    public static PluginContext pluginContext() {
        return SINGLETON;
    }

    private SlimTableFactory slimTableFactory;
}
