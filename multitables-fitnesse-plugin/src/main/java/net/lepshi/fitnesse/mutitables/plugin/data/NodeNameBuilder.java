package net.lepshi.fitnesse.mutitables.plugin.data;

import net.lepshi.fitnesse.mutitables.plugin.data.ObjectStructureNodes.*;
import lombok.RequiredArgsConstructor;

import static com.google.common.base.Preconditions.checkState;
import static java.lang.String.format;
import static java.util.Objects.nonNull;

@RequiredArgsConstructor
class NodeNameBuilder {

    private final Node node;

    String buildName() {
        Node child = node;
        Node parent = node.parent;

        if (parent == null) {
            return "ROOT";
        }

        String name = "";

        while (parent != null) {
            name = parent.handleBy(new ParentNameAdder(child, name));

            child = parent;
            parent = parent.parent;
        }
        return name;
    }


    @RequiredArgsConstructor
    private class ParentNameAdder implements NodeHandler<String> {

        private final Node   childNode;
        private final String childName;

        @Override
        public String handle(SimpleNode simpleNode) {
            return "";
        }

        @Override
        public String handle(CompositeNode compositeNode) {
            String name = compositeNode.children.inverse().get(childNode);
            checkState(nonNull(name), "Node is not registered in its parent: %s", childNode);

            boolean isOnRootNode = (compositeNode.parent == null);
            return format(isOnRootNode ? "%s%s" : "{%s%s}", name, childName);
        }

        @Override
        public String handle(ArrayNode arrayNode) {
            return "[]" + childName;
        }
    }
}
