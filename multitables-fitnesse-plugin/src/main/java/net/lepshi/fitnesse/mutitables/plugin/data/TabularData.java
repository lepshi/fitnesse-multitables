package net.lepshi.fitnesse.mutitables.plugin.data;

import com.google.gson.Gson;
import com.google.gson.stream.JsonWriter;
import fitnesse.testsystems.slim.Table;

import java.io.IOException;
import java.io.StringWriter;

import static com.google.common.base.Preconditions.checkArgument;


public class TabularData {

    private final Gson                      gson = new Gson();
    private final Table                     dataTable;
    private final ObjectStructureNodes.Node rootField;

    public TabularData(Table dataTable) {
        this.dataTable = dataTable;
        checkArgument(dataTable.getRowCount() >= 2, "Tabular data must consist of one header row and at least one data row");
        this.rootField = buildObjectRootField(dataTable);
    }

    private ObjectStructureNodes.Node buildObjectRootField(Table dataTable) {
        return new ObjectStructureBuilder(dataTable).buildObjectRoot();
    }


    public String asJson() {
        try {
            StringWriter writer = new StringWriter();
            writeTableAsJson(gson.newJsonWriter(writer));
            return writer.toString();

        } catch (IOException e) {
            throw new RuntimeException("Unexpected IO error", e);
        }
    }

    private void writeTableAsJson(JsonWriter jsonWriter) {
        new TableAsJsonWriter(dataTable, jsonWriter).writeAsJson(rootField);
    }
}
