package net.lepshi.fitnesse.mutitables.plugin.common;

import fitnesse.testsystems.slim.results.SlimExceptionResult;
import fitnesse.testsystems.slim.results.SlimTestResult;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

class UnsupportedOperationsTableTest {

    private UnsupportedOperationsTable table = new UnsupportedOperationsTable();

    @Test
    void should_throw_UnsupportedOperationException_for_all_interface_methods() {
        assertThrows(UnsupportedOperationException.class, () -> table.isTearDown());
        assertThrows(UnsupportedOperationException.class, () -> table.getCellContents(0, 0));
        assertThrows(UnsupportedOperationException.class, () -> table.getRowCount());
        assertThrows(UnsupportedOperationException.class, () -> table.getColumnCountInRow(0));
        assertThrows(UnsupportedOperationException.class, () -> table.substitute(0, 0, null));
        assertThrows(UnsupportedOperationException.class, () -> table.addRow(null));
        assertThrows(UnsupportedOperationException.class, () -> table.addColumnToRow(0, null));
        assertThrows(UnsupportedOperationException.class, () -> table.appendChildTable(0, null));
        assertThrows(UnsupportedOperationException.class, () -> table.updateContent(0, null));
        assertThrows(UnsupportedOperationException.class, () -> table.updateContent(0, 0, (SlimTestResult) null));
        assertThrows(UnsupportedOperationException.class, () -> table.updateContent(0, 0, (SlimExceptionResult) null));
        assertThrows(UnsupportedOperationException.class, () -> table.asTemplate(null));
    }
}