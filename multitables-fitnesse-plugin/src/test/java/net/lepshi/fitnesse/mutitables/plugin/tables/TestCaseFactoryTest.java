package net.lepshi.fitnesse.mutitables.plugin.tables;

import fitnesse.testsystems.ExecutionResult;
import fitnesse.testsystems.TestExecutionException;
import fitnesse.testsystems.slim.SlimTestContext;
import fitnesse.testsystems.slim.Table;
import fitnesse.testsystems.slim.results.SlimTestResult;
import fitnesse.testsystems.slim.tables.SlimAssertion;
import fitnesse.testsystems.slim.tables.SlimTable;
import fitnesse.testsystems.slim.tables.SlimTableFactory;
import net.lepshi.fitnesse.mutitables.plugin.PluginContextTestAccess;
import net.lepshi.fitnesse.mutitables.plugin.common.StringTable;
import net.lepshi.fitnesse.mutitables.plugin.tables.ActionCombination.CombinationType;
import net.lepshi.fitnesse.mutitables.plugin.tables.AllExpectationsEvaluatedNotifyingCustomizer.AllExpectationsEvaluatedCallback;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static fitnesse.testsystems.ExecutionResult.FAIL;
import static java.util.Arrays.asList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class TestCaseFactoryTest {

    private final PluginContextTestAccess contextTestAccess = new PluginContextTestAccess();

    @Mock
    private SlimTableFactory slimTableFactory;
    @Mock
    private SlimTestContext  slimTestContext;
    private TestCaseFactory  factory;

    @Mock
    private Table             titleTable;
    @Mock
    private Table             combinationReportingTable;
    private List<Table>       actionTables = new ArrayList<>();
    private ActionCombination actionCombination;


    @BeforeEach
    void init() {
        MockitoAnnotations.initMocks(this);
        contextTestAccess.setSlimTableFactory(slimTableFactory);
        factory = new TestCaseFactory(new EachTestSteps(), slimTestContext) {
            @Override
            AssertionsCustomizer assertionsCustomizer(Table titleTable, Table stepsReportingTable) {
                return assertions -> assertions;
            }
        };
    }


    @Test
    void should_declare_combination_type_TEST_CASE() {
        assertThat(factory.getCombinationType(), is(CombinationType.TEST_CASE));
    }

    @Test
    void should_declare_tables_received_in_constructor() {
        actionTables.add(mock(Table.class));
        actionTables.add(mock(Table.class));

        createActionCombination();

        assertThat(actionCombination.getTitleTable(), is(titleTable));
        assertThat(actionCombination.getActionTables(), contains(actionTables.get(0), actionTables.get(1)));
    }

    @Test
    void should_declare_combination_type_TEST_CASE_from_combination() {
        createActionCombination();

        assertThat(actionCombination.getCombinationType(), is(CombinationType.TEST_CASE));
    }

    @Test
    void should_provide_combination_title_from_title_table() {
        titleTable = StringTable.from("|Test Case Title|TC Description|");

        createActionCombination();

        assertThat(actionCombination.getCombinationTitle(), is("Test Case Title"));
    }

    @Test
    void should_assign_id_from_increasing_sequence_for_each_combination_created() {
        createActionCombination();
        assertThat(actionCombination.getCombinationIdSupplier().get(), is("tc0"));
        createActionCombination();
        assertThat(actionCombination.getCombinationIdSupplier().get(), is("tc1"));
        createActionCombination();
        assertThat(actionCombination.getCombinationIdSupplier().get(), is("tc2"));
    }

    @Test
    void should_get_assertions_from_action_tables_when_basic_use() throws TestExecutionException {
        actionTables.add(mock(Table.class));
        actionTables.add(mock(Table.class));

        final SlimTable slimTable_0 = mock(SlimTable.class);
        final SlimTable slimTable_1 = mock(SlimTable.class);
        when(slimTableFactory.makeSlimTable(actionTables.get(0), "action[0]@tc0", slimTestContext)).thenReturn(slimTable_0);
        when(slimTableFactory.makeSlimTable(actionTables.get(1), "action[1]@tc0", slimTestContext)).thenReturn(slimTable_1);

        final SlimAssertion assertion_0_0 = mock(SlimAssertion.class);
        final SlimAssertion assertion_0_1 = mock(SlimAssertion.class);
        final SlimAssertion assertion_1_0 = mock(SlimAssertion.class);
        final SlimAssertion assertion_1_1 = mock(SlimAssertion.class);
        when(slimTable_0.getAssertions()).thenReturn(asList(assertion_0_0, assertion_0_1));
        when(slimTable_1.getAssertions()).thenReturn(asList(assertion_1_0, assertion_1_1));

        createActionCombination();

        assertThat(actionCombination.getAssertions(), contains(assertion_0_0,
                                                               assertion_0_1,
                                                               assertion_1_0,
                                                               assertion_1_1));

        assertThat(actionCombination.actionIdOf(assertion_0_0), is("0"));
        assertThat(actionCombination.actionIdOf(assertion_0_1), is("0"));
        assertThat(actionCombination.actionIdOf(assertion_1_0), is("1"));
        assertThat(actionCombination.actionIdOf(assertion_1_1), is("1"));
    }

    @Test
    void should_update_title_table_when_fail_result() {
        when(titleTable.getRowCount()).thenReturn(1);
        when(titleTable.getColumnCountInRow(0)).thenReturn(2);

        AllExpectationsEvaluatedCallback evaluationCallback = factory.testCaseActionsEvaluator(titleTable);

        SlimTestResult fail = SlimTestResult.fail();
        evaluationCallback.notifyAllExpectationsEvaluated(asList(SlimTestResult.pass(),
                                                                 fail,
                                                                 SlimTestResult.ignore()));

        verify(titleTable, times(2)).updateContent(anyInt(), anyInt(), any(SlimTestResult.class));
        verify(titleTable).updateContent(eq(0), eq(0), isTestResult(FAIL));
        verify(titleTable).updateContent(eq(1), eq(0), isTestResult(FAIL));
    }


    private void createActionCombination() {
        actionCombination = factory.createActionCombination(titleTable, combinationReportingTable, actionTables);
    }

    private SlimTestResult isTestResult(ExecutionResult executionResult) {
        return argThat(result -> result.getExecutionResult() == executionResult);
    }
}