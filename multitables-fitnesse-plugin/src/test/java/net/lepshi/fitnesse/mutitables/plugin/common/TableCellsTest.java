package net.lepshi.fitnesse.mutitables.plugin.common;

import net.lepshi.fitnesse.mutitables.plugin.data.Rows;
import org.junit.jupiter.api.Test;

import java.util.List;

import static java.lang.String.format;
import static java.util.stream.Collectors.toList;
import static net.lepshi.fitnesse.mutitables.plugin.common.TableCells.cellsOf;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.junit.jupiter.api.Assertions.assertThrows;

class TableCellsTest {

    private TableCells tableCells;

    @Test
    void should_fail_when_null_table() {
        assertThrows(NullPointerException.class, () -> cellsOf(null));
    }

    @Test
    void should_return_single_cell_when_minimal_table() {
        tableCells = cellsOf(StringTable.from("||"));

        assertCells("(0,0)");
    }

    @Test
    void should_return_cells_when_complex_table() {
        tableCells = cellsOf(StringTable.from("|a0      |",
                                              "|b0|b1|b2|",
                                              "|  |c1   |"));

        assertCells("(0,0)",
                    "(0,1)", "(1,1)", "(2,1)",
                    "(0,2)", "(1,2)");
    }

    @Test
    void should_return_cells_in_chosen_table_rows_only() {
        tableCells = cellsOf(StringTable.from("|a0      |",
                                              "|b0|b1|b2|",
                                              "|c0      |",
                                              "|d0|d1   |"),
                             Rows.range(1, 2));

        assertCells("(0,1)", "(1,1)", "(2,1)",
                    "(0,2)");
    }

    private void assertCells(String... cells) {
        List<String> actualCells = tableCells.asList()
                                             .stream()
                                             .map(cell -> format("(%d,%d)", cell.col, cell.row))
                                             .collect(toList());
        assertThat(actualCells, contains(cells));
    }


}