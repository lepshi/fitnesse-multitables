package net.lepshi.fitnesse.mutitables.plugin.tables;

import fitnesse.testsystems.ExecutionResult;
import fitnesse.testsystems.slim.results.SlimTestResult;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Stream;

import static fitnesse.testsystems.ExecutionResult.FAIL;
import static fitnesse.testsystems.ExecutionResult.IGNORE;
import static fitnesse.testsystems.ExecutionResult.PASS;
import static fitnesse.testsystems.slim.results.SlimTestResult.fail;
import static fitnesse.testsystems.slim.results.SlimTestResult.ignore;
import static fitnesse.testsystems.slim.results.SlimTestResult.pass;
import static fitnesse.testsystems.slim.results.SlimTestResult.plain;
import static java.util.Arrays.asList;
import static java.util.function.Function.identity;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;


class ResultsPickerTest {

    private static final Function ADJUSTMENT_IGNORE_TO_FAIL = result -> (result == IGNORE) ? FAIL
                                                                                           : result;

    private static Stream<Arguments> params() {
        return Stream.of(
                Arguments.of(asList(pass(), pass()), identity(), PASS),
                Arguments.of(asList(pass(), fail(), pass()), identity(), FAIL),
                Arguments.of(asList(ignore(), pass(), fail()), identity(), FAIL),
                Arguments.of(asList(ignore(), pass()), identity(), IGNORE),
                Arguments.of(asList(plain(), plain()), identity(), PASS),
                Arguments.of(asList(ignore(), plain()), identity(), IGNORE),
                Arguments.of(asList(ignore(), plain()), ADJUSTMENT_IGNORE_TO_FAIL, FAIL)
        );
    }

    @ParameterizedTest
    @MethodSource("params")
    void should_pick_worst(List<SlimTestResult> testResults,
                           Function<ExecutionResult, ExecutionResult> adjustment,
                           ExecutionResult expectedPick) {

        ExecutionResult worst = ResultsPicker.pickFromResults(testResults)
                                             .adjusted(adjustment)
                                             .worst();
        assertThat(worst, is(expectedPick));
    }
}