package net.lepshi.fitnesse.mutitables.plugin.tables;

import fitnesse.testsystems.ExecutionResult;
import fitnesse.testsystems.slim.Table;
import fitnesse.testsystems.slim.results.SlimExceptionResult;
import fitnesse.testsystems.slim.results.SlimTestResult;
import net.lepshi.fitnesse.mutitables.plugin.TableException;
import net.lepshi.fitnesse.mutitables.plugin.common.Cell;
import net.lepshi.fitnesse.mutitables.plugin.common.StringTable;
import net.lepshi.fitnesse.mutitables.plugin.tables.ActionCombination.CombinationType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;

import static java.util.stream.Collectors.toList;
import static net.lepshi.fitnesse.mutitables.plugin.common.Cell.cell;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


class HorizontalActionCombinationDiscoveryTest {

    private static final CombinationType COMBINATION_TYPE = CombinationType.TEST_CASE;

    private StringTable              table;
    @Mock
    private ActionCombinationFactory actionCombinationFactory;

    private HorizontalActionCombinationDiscovery discovery;

    private List<StringTable>       actualTitleTables;
    private List<StringTable>       actualCombinationReportingTables;
    private List<List<StringTable>> actualCombinationActionTables;

    @BeforeEach
    void init() {
        MockitoAnnotations.initMocks(this);
    }

    private void givenTableRows(String... rows) {
        table = spy(StringTable.from(rows));
        doNothing().when(table).updateContent(anyInt(), anyInt(), any(SlimTestResult.class));
        doNothing().when(table).updateContent(anyInt(), anyInt(), any(SlimExceptionResult.class));
        when(actionCombinationFactory.getCombinationType()).thenReturn(COMBINATION_TYPE);
        discovery = new HorizontalActionCombinationDiscovery(table,
                                                             actionCombinationFactory);
    }


    @Test
    void should_discover_minimal_combination() {
        givenTableRows("|multiTable:horizontal|",
                       "|   |action           |",
                       "|TC |Description      |",
                       "|   |data             |");

        discoverActionCombinations(1);

        assertActionCombinationsTables(0,
                                       1,
                                       StringTable.from("|TC|Description|"),
                                       StringTable.from("|action        |",
                                                        "|data          |"));
    }

    @Test
    void should_discover_when_fixed_rows() {
        givenTableRows("|multiTable:horizontal|",
                       "|     |action         |",
                       "|     |fixedData      |",
                       "|TC 1 |Desc 1         |",
                       "|     |variableData1  |",
                       "|TC 2 |Desc 2         |",
                       "|     |variableData2  |");

        discoverActionCombinations(2);

        assertActionCombinationsTables(0,
                                       1,
                                       StringTable.from("|TC 1|Desc 1|"),
                                       StringTable.from("|action       |",
                                                        "|fixedData    |",
                                                        "|variableData1|"));
        assertActionCombinationsTables(1,
                                       1,
                                       StringTable.from("|TC 2|Desc 2|"),
                                       StringTable.from("|action       |",
                                                        "|fixedData    |",
                                                        "|variableData2|"));
    }

    @Test
    void should_discover_complex_combinations() {
        givenTableRows("|multiTable:horizontal                                                         |",
                       "|    |given       |            |when       |           |then                   |",
                       "|    |fixedGiven a|fixedGiven b|fixedWhen a|fixedWhen b|fixedThen a|fixedThen b|",
                       "|TC 1|Desc 1                                                                   |",
                       "|    |varGiven 1a |varGiven 1b |varWhen 1a |varWhen 1b |varThen 1a |varThen 1b |",
                       "|TC 2|Desc 2                                                                   |",
                       "|    |varGiven 2a |varGiven 2b |varWhen 2ax|varWhen 2bx|varThen 2a |varThen 2b |",
                       "|    |varGiven 2ax|            |           |           |           |varThen 2bx|",
                       "|TC 3|Desc 3                                                                   |",
                       "|    |varGiven 3a |varGiven 3b |varWhen 3a |varWhen 3b |varThen 3a |varThen 3b |");

        discoverActionCombinations(3);

        assertActionCombinationsTables(0,
                                       1,
                                       StringTable.from("|TC 1|Desc 1|"),
                                       StringTable.from("|given                    |",
                                                        "|fixedGiven a|fixedGiven b|",
                                                        "|varGiven 1a |varGiven 1b |"),
                                       StringTable.from("|when                     |",
                                                        "|fixedWhen a|fixedWhen b|",
                                                        "|varWhen 1a |varWhen 1b |"),
                                       StringTable.from("|then                   |",
                                                        "|fixedThen a|fixedThen b|",
                                                        "|varThen 1a |varThen 1b |"));

        assertActionCombinationsTables(1,
                                       2,
                                       StringTable.from("|TC 2|Desc 2|"),
                                       StringTable.from("|given                    |",
                                                        "|fixedGiven a|fixedGiven b|",
                                                        "|varGiven 2a |varGiven 2b |",
                                                        "|varGiven 2ax|            |"),
                                       StringTable.from("|when                     |",
                                                        "|fixedWhen a|fixedWhen b|",
                                                        "|varWhen 2ax|varWhen 2bx|",
                                                        "|           |           |"),
                                       StringTable.from("|then                   |",
                                                        "|fixedThen a|fixedThen b|",
                                                        "|varThen 2a |varThen 2b |",
                                                        "|           |varThen 2bx|"));

        assertActionCombinationsTables(2,
                                       1,
                                       StringTable.from("|TC 3|Desc 3|"),
                                       StringTable.from("|given                    |",
                                                        "|fixedGiven a|fixedGiven b|",
                                                        "|varGiven 3a |varGiven 3b |"),
                                       StringTable.from("|when                     |",
                                                        "|fixedWhen a|fixedWhen b|",
                                                        "|varWhen 3a |varWhen 3b |"),
                                       StringTable.from("|then                   |",
                                                        "|fixedThen a|fixedThen b|",
                                                        "|varThen 3a |varThen 3b |"));
    }

    @Test
    void should_fail_to_discover_when_no_at_least_4_rows() {
        givenTableRows("|multiTable:horizontal|",
                       "|                     |",
                       "|                     |");

        TableException thrown = assertThrows(TableException.class, () -> discoverActionCombinations(-1));
        assertThat(thrown.getMessage(), is("At least 4 rows expected (tableType, actions, title, data)"));

        assertTableExceptionHighlighting(thrown, 0, 1);
        assertExceptionResult(cell(0, 0), "message:<<At least 4 rows expected>>");
    }

    @Test
    void should_fail_to_discover_when_actions_row_too_short() {
        givenTableRows("|multiTable:horizontal|",
                       "|                     |",
                       "|                     |",
                       "|                     |");

        TableException thrown = assertThrows(TableException.class, () -> discoverActionCombinations(-1));
        assertThat(thrown.getMessage(), is("Actions row must have at least 2 columns"));

        assertTableExceptionHighlighting(thrown, 0, 1);
        assertExceptionResult(cell(0, 1), "message:<<At least 2 columns expected>>");
    }

    @Test
    void should_fail_to_discover_when_wrong_action_row() {
        givenTableRows("|multiTable:horizontal|",
                       "|WRONG |action        |",
                       "|TC    |Desc          |",
                       "|      |data          |");

        TableException thrown = assertThrows(TableException.class, () -> discoverActionCombinations(-1));
        assertThat(thrown.getMessage(), is("Column 0 in action type row must be empty"));

        assertTableExceptionHighlighting(thrown, 0, 1);
        assertExceptionResult(cell(0, 1), "message:<<Cell must be empty>>");
    }

    @Test
    void should_fail_to_discover_when_first_action_missing() {
        givenTableRows("|multiTable:horizontal|",
                       "|  |  |nonFirstAction |",
                       "|TC|Desc              |",
                       "|  |  |data           |");

        TableException thrown = assertThrows(TableException.class, () -> discoverActionCombinations(-1));
        assertThat(thrown.getMessage(), is("First action type expected in column 1"));

        assertTableExceptionHighlighting(thrown, 0, 1);
        assertExceptionResult(cell(1, 1), "message:<<First action expected here>>");
    }

    @Test
    void should_fail_to_discover_when_no_combinations() {
        givenTableRows("|multiTable:horizontal|",
                       "|  |action            |",
                       "|  |foo               |",
                       "|  |bar               |");

        TableException thrown = assertThrows(TableException.class, () -> discoverActionCombinations(-1));
        assertThat(thrown.getMessage(), is("No " + COMBINATION_TYPE.getDisplayName() + " found in column 0"));

        assertTableExceptionHighlighting(thrown, 0, 1);
        assertExceptionResult(cell(0, 3), "message:<<Any " + COMBINATION_TYPE.getDisplayName() + "?>>");
    }

    @Test
    void should_fail_to_discover_when_no_action_data_for_non_last_test() {
        givenTableRows("|multiTable:horizontal|",
                       "|      |action        |",
                       "|TC 1  |Desc          |",
                       "|TC 2  |Desc          |",
                       "|      |data          |");

        TableException thrown = assertThrows(TableException.class, () -> discoverActionCombinations(-1));
        assertThat(thrown.getMessage(), is(COMBINATION_TYPE.getDisplayName() + " 'TC 1' must have at least 1 data row"));

        assertTableExceptionHighlighting(thrown, 0, 1);
        assertExceptionResult(cell(0, 2), "message:<<Missing data rows>>");
    }

    @Test
    void should_fail_to_discover_when_no_action_data_for_last_test() {
        givenTableRows("|multiTable:horizontal|",
                       "|      |action        |",
                       "|TC 1  |Desc          |",
                       "|      |data          |",
                       "|TC 2  |Desc          |");

        TableException thrown = assertThrows(TableException.class, () -> discoverActionCombinations(-1));
        assertThat(thrown.getMessage(), is(COMBINATION_TYPE.getDisplayName() + " 'TC 2' must have at least 1 data row"));

        assertTableExceptionHighlighting(thrown, 0, 1);
        assertExceptionResult(cell(0, 4), "message:<<Missing data rows>>");
    }


    @SuppressWarnings("unchecked")
    private void discoverActionCombinations(int expectedCount) {
        assertThat(discovery.discoverActionCombinations(), hasSize(expectedCount));

        final ArgumentCaptor<Table> titleTableCaptor = ArgumentCaptor.forClass(Table.class);
        final ArgumentCaptor<Table> combinationReportingTableCaptor = ArgumentCaptor.forClass(Table.class);
        final ArgumentCaptor<List<Table>> actionTablesCaptor = ArgumentCaptor.forClass(List.class);

        verify(actionCombinationFactory, times(expectedCount)).createActionCombination(titleTableCaptor.capture(),
                                                                                       combinationReportingTableCaptor.capture(),
                                                                                       actionTablesCaptor.capture());

        actualTitleTables = titleTableCaptor.getAllValues()
                                            .stream()
                                            .map(StringTable::from)
                                            .collect(toList());
        actualCombinationReportingTables = combinationReportingTableCaptor.getAllValues()
                                                                          .stream()
                                                                          .map(StringTable::from)
                                                                          .collect(toList());
        actualCombinationActionTables = actionTablesCaptor.getAllValues()
                                                          .stream()
                                                          .map(list -> list.stream()
                                                                           .map(StringTable::from)
                                                                           .collect(toList()))
                                                          .collect(toList());

        assertThat(actualTitleTables, hasSize(expectedCount));
        assertThat(actualCombinationReportingTables, hasSize(expectedCount));
        assertThat(actualCombinationActionTables, hasSize(expectedCount));
    }

    private void assertActionCombinationsTables(int combinationIdx,
                                                int reportingRows,
                                                StringTable titleTable,
                                                StringTable... actionTables) {

        final StringTable actualTitleTable = actualTitleTables.get(combinationIdx);
        final StringTable actualCombinationReportingTable = actualCombinationReportingTables.get(combinationIdx);
        final List<StringTable> actualActionTables = actualCombinationActionTables.get(combinationIdx);

        assertThat(actualTitleTable, is(titleTable));
        assertThat(actualCombinationReportingTable, is(StringTable.empty(1, reportingRows)));
        assertThat(actualActionTables, hasSize(actionTables.length));

        for (int idx = 0; idx < actionTables.length; idx++) {
            assertThat(actualActionTables.get(idx), is(actionTables[idx]));
        }
    }


    private void assertTableExceptionHighlighting(TableException thrown, int countResults, int countExceptions) {
        verify(table, never()).updateContent(anyInt(), anyInt(), any(SlimTestResult.class));
        verify(table, never()).updateContent(anyInt(), anyInt(), any(SlimExceptionResult.class));

        thrown.highlightTableData();

        verify(table, times(countResults)).updateContent(anyInt(), anyInt(), any(SlimTestResult.class));
        verify(table, times(countExceptions)).updateContent(anyInt(), anyInt(), any(SlimExceptionResult.class));
    }

    private void assertTestResult(Cell cell, ExecutionResult cellExecutionResult, String cellMessage) {
        final ArgumentCaptor<SlimTestResult> testResultCaptor = ArgumentCaptor.forClass(SlimTestResult.class);

        verify(table).updateContent(eq(cell.col), eq(cell.row), testResultCaptor.capture());

        final SlimTestResult actualTestResult = testResultCaptor.getValue();
        assertThat(actualTestResult.getExecutionResult(), is(cellExecutionResult));
        assertThat(actualTestResult.getMessage(), is(cellMessage));
    }

    private void assertExceptionResult(Cell cell, String errorMessage) {
        final ArgumentCaptor<SlimExceptionResult> exceptionResultCaptor = ArgumentCaptor.forClass(SlimExceptionResult.class);

        verify(table).updateContent(eq(cell.col), eq(cell.row), exceptionResultCaptor.capture());

        final SlimExceptionResult actualExceptionResult = exceptionResultCaptor.getValue();
        assertThat(actualExceptionResult.getException(), is(errorMessage));
    }
}