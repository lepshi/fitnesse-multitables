package net.lepshi.fitnesse.mutitables.plugin.data;

import lombok.Data;

import java.util.List;

@Data
class BookOrdersHistory {

    private List<Order> orders;

    @Data
    static class Order {
        private boolean  isGift;
        private Shipping shipping;
        private Book[]   books;
    }

    @Data
    static class Shipping {
        private String name;
        private String address;
        private double cost;
    }

    @Data
    static class Book {
        private String title;
        private String author;
        private int    quantity;
        private double price;
    }
}
