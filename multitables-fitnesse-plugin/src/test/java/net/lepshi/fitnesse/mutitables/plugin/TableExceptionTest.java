package net.lepshi.fitnesse.mutitables.plugin;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

class TableExceptionTest {

    @Mock
    private TableDataHighlighter highlighter;
    private TableException       exception;


    @BeforeEach
    void init() {
        MockitoAnnotations.initMocks(this);
        exception = new TableException(highlighter, "");
    }


    @Test
    void should_not_highlight_when_constructed() {
        verify(highlighter, never()).highlight();
    }

    @Test
    void should_highlight_when_explicitly_requested() {
        exception.highlightTableData();
        verify(highlighter, times(1)).highlight();
    }

    @Test
    void should_highlight_only_once_when_requested_multiple_times() {
        exception.highlightTableData();
        exception.highlightTableData();
        exception.highlightTableData();
        verify(highlighter, times(1)).highlight();
    }
}