package net.lepshi.fitnesse.mutitables.plugin.tables;

import fitnesse.testsystems.ExecutionResult;
import fitnesse.testsystems.TestResult;
import fitnesse.testsystems.slim.results.SlimExceptionResult;
import fitnesse.testsystems.slim.tables.SlimAssertion;
import fitnesse.testsystems.slim.tables.SlimExpectation;
import net.lepshi.fitnesse.mutitables.plugin.tables.AllExpectationsEvaluatedNotifyingCustomizer.AllExpectationsEvaluatedCallback;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;

import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.Matchers.sameInstance;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

class AllExpectationsEvaluatedNotifyingCustomizerTest {

    private static final TestResult          TEST_RESULT      = mock(TestResult.class);
    private static final SlimExceptionResult EXCEPTION_RESULT = mock(SlimExceptionResult.class);

    @Mock
    private SlimExpectation                  underlying1;
    @Mock
    private SlimExpectation                  underlying2;
    @Mock
    private AllExpectationsEvaluatedCallback callback;
    private AssertionsCustomizer             customizer;
    private List<SlimExpectation>            wrappedExpectations;


    @BeforeEach
    void init() {
        MockitoAnnotations.initMocks(this);

        when(underlying1.evaluateException(any())).then(invocation -> invocation.getArgument(0));
        when(underlying2.evaluateException(any())).then(invocation -> invocation.getArgument(0));

        customizer = new AllExpectationsEvaluatedNotifyingCustomizer(callback);
        List<SlimAssertion> wrappedAssertions = customizer.customize(asList(new SlimAssertion(null, underlying1),
                                                                            new SlimAssertion(null, underlying2)));
        wrappedExpectations = wrappedAssertions.stream()
                                               .map(SlimAssertion::getExpectation)
                                               .collect(toList());
    }


    @Test
    void should_create_3_wrapped_expectation_when_3_underlying() {
        List<SlimAssertion> wrappedAssertions = customizer.customize(asList(new SlimAssertion(null, underlying1),
                                                                            new SlimAssertion(null, underlying2),
                                                                            new SlimAssertion(null, underlying2)));
        assertThat(wrappedAssertions, hasSize(3));
    }

    @Test
    void should_delegate_to_underlying_when_wrapped_expectation_called() {
        final Object RETURN_VALUES = mock(Object.class);

        assertThat(wrappedExpectations, hasSize(2));

        when(underlying1.evaluateExpectation(RETURN_VALUES)).thenReturn(TEST_RESULT);

        assertThat(wrappedExpectations.get(0).evaluateExpectation(RETURN_VALUES), is(TEST_RESULT));
        assertThat(wrappedExpectations.get(1).evaluateException(EXCEPTION_RESULT), is(EXCEPTION_RESULT));

        verify(underlying1).evaluateExpectation(RETURN_VALUES);
        verify(underlying2).evaluateException(EXCEPTION_RESULT);

        verifyNoMoreInteractions(underlying1, underlying2);
    }

    @Test
    void should_not_notify_when_not_all_expectations_were_evaluated() {
        wrappedExpectations.get(0).evaluateExpectation("result");

        verify(callback, never()).notifyAllExpectationsEvaluated(any());
    }

    @Test
    void should_notify_when_all_expectations_were_evaluated() {
        final TestResult TEST_RESULT_2 = mock(TestResult.class);
        when(underlying1.evaluateExpectation(any())).thenReturn(TEST_RESULT);
        when(underlying2.evaluateExpectation(any())).thenReturn(TEST_RESULT_2);

        wrappedExpectations.get(0).evaluateExpectation("some result 1");
        wrappedExpectations.get(1).evaluateExpectation("some result 2");

        verify(callback).notifyAllExpectationsEvaluated(asList(TEST_RESULT, TEST_RESULT_2));
    }

    @Test
    void should_hide_exception_when_callback_function_blows() {
        when(underlying1.evaluateExpectation(any())).thenReturn(TEST_RESULT);
        when(underlying2.evaluateExpectation(any())).thenReturn(TEST_RESULT);
        doThrow(RuntimeException.class).when(callback).notifyAllExpectationsEvaluated(any());

        wrappedExpectations.get(0).evaluateExpectation("some result 1");
        wrappedExpectations.get(1).evaluateExpectation("some result 2");

        verify(callback).notifyAllExpectationsEvaluated(any());
    }

    @Test
    @SuppressWarnings("unchecked")
    void should_notify_when_all_expectations_were_evaluated_some_with_exceptions() {
        when(underlying1.evaluateExpectation(any())).thenReturn(TEST_RESULT);

        wrappedExpectations.get(0).evaluateExpectation("some result 1");
        wrappedExpectations.get(1).evaluateException(new SlimExceptionResult("exKey", "exValue"));

        final ArgumentCaptor<List<TestResult>> testResultsCaptor = ArgumentCaptor.forClass(List.class);
        verify(callback).notifyAllExpectationsEvaluated(testResultsCaptor.capture());

        List<TestResult> actualTestResults = testResultsCaptor.getValue();
        assertThat(actualTestResults, hasSize(2));
        assertThat(actualTestResults.get(0), is(sameInstance(TEST_RESULT)));
        assertThat(actualTestResults.get(1).getExecutionResult(), is(ExecutionResult.ERROR));
        assertThat(actualTestResults.get(1).getMessage(), is("exValue"));
    }

    @Test
    @SuppressWarnings("unchecked")
    void should_convert_to_plain_when_null_test_result() {
        when(underlying1.evaluateExpectation(any())).thenReturn(TEST_RESULT);
        when(underlying2.evaluateExpectation(any())).thenReturn(null);

        wrappedExpectations.get(0).evaluateExpectation("dummy result 1");
        wrappedExpectations.get(1).evaluateExpectation("dummy result 2");

        final ArgumentCaptor<List<TestResult>> testResultsCaptor = ArgumentCaptor.forClass(List.class);
        verify(callback).notifyAllExpectationsEvaluated(testResultsCaptor.capture());

        List<TestResult> actualTestResults = testResultsCaptor.getValue();
        assertThat(actualTestResults.get(1).getExecutionResult(), is(nullValue()));
        assertThat(actualTestResults.get(1).getMessage(), is(nullValue()));
    }

}