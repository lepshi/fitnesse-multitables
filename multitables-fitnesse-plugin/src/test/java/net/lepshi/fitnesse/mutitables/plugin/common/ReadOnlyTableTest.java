package net.lepshi.fitnesse.mutitables.plugin.common;

import fitnesse.testsystems.slim.Table;
import fitnesse.testsystems.slim.results.SlimExceptionResult;
import fitnesse.testsystems.slim.results.SlimTestResult;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;

class ReadOnlyTableTest {

    @Mock
    private Table table;
    private Table readOnlyTable;


    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        readOnlyTable = ReadOnlyTable.readOnly(table);
    }


    @Test
    void should_delegate_readonly_operations() {
        readOnlyTable.getCellContents(1, 2);
        verify(table).getCellContents(1, 2);

        readOnlyTable.getColumnCountInRow(123);
        verify(table).getColumnCountInRow(123);

        readOnlyTable.getRowCount();
        verify(table).getRowCount();

        readOnlyTable.isTearDown();
        verify(table).isTearDown();

        verifyNoMoreInteractions(table);
    }

    @Test
    void should_not_update_underlying() {
        readOnlyTable.updateContent(1, null);
        readOnlyTable.updateContent(1, 2, (SlimTestResult) null);
        readOnlyTable.updateContent(1, 2, (SlimExceptionResult) null);
        readOnlyTable.substitute(1, 2, "newContent");

        verifyNoInteractions(table);
    }
}