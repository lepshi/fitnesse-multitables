package net.lepshi.fitnesse.mutitables.plugin.common;

import fitnesse.testsystems.slim.results.SlimExceptionResult;
import fitnesse.testsystems.slim.results.SlimTestResult;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

class SubTableTest {

    private static final SlimTestResult      TEST_RESULT      = mock(SlimTestResult.class);
    private static final SlimExceptionResult EXCEPTION_RESULT = mock(SlimExceptionResult.class);

    @Spy
    private StringTable baseTable = StringTable.from("|a0|a1   |",
                                                     "|b0      |",
                                                     "|c0|c1|c2|");

    @BeforeEach
    void init() {
        MockitoAnnotations.initMocks(this);
    }

    private SubTable subTable(int firstCol, int lastCol,
                              int firstRow, int lastRow) {
        return new SubTable(baseTable,
                            firstCol, lastCol,
                            firstRow, lastRow);
    }


    @Test
    void should_construct_when_valid_subtable() {

        final SubTable subTable = subTable(0, 0, 0, 0);

        assertThat(StringTable.from(subTable), is(StringTable.from("|a0|")));
    }

    @Test
    void should_fail_to_construct_when_subtable_doesnt_fit_inside_baseTable() {

        subTable(0, 999, 0, 0); //no upfront check on max columns
        assertThrows(RuntimeException.class, () -> subTable(2, 2, 0, 0));
        assertThrows(RuntimeException.class, () -> subTable(0, 0, 0, 3));
        assertThrows(RuntimeException.class, () -> subTable(0, 0, 3, 3));
        assertThrows(RuntimeException.class, () -> subTable(1, 1, 1, 1));
    }

    @Test
    void should_directly_delegate_when_isTearDown() {
        doReturn(true).when(baseTable).isTearDown();

        assertThat(subTable(0, 0, 0, 0).isTearDown(), is(true));

        verify(baseTable).isTearDown();
    }

    @Test
    void should_map_to_delegate_when_getCellContents() {
        final SubTable subTable = subTable(0, 1, 1, 2);

        assertThat(subTable.getCellContents(0, 0), is("b0"));
        assertThat(subTable.getCellContents(1, 1), is("c1"));
        assertThrows(RuntimeException.class, () -> subTable.getCellContents(2, 1));
        assertThrows(RuntimeException.class, () -> subTable.getCellContents(1, 2));
    }

    @Test
    void should_report_adjusted_getRowCount() {
        final SubTable subTable = subTable(0, 0, 1, 2);
        assertThat(subTable.getRowCount(), is(2));
    }

    @Test
    void should_report_adjusted_getColumnCountInRow() {
        final SubTable subTable = subTable(0, 2, 1, 2);

        assertThat(subTable.getColumnCountInRow(0), is(1));
        assertThat(subTable.getColumnCountInRow(1), is(3));
        assertThrows(RuntimeException.class, () -> subTable.getColumnCountInRow(2));
    }

    @Test
    void should_map_to_delegate_when_substitute() {
        final SubTable subTable = subTable(0, 2, 1, 2);

        subTable.substitute(0, 0, "CONTENT1");
        subTable.substitute(1, 1, "CONTENT2");
        assertThrows(RuntimeException.class, () -> subTable.substitute(1, 0, "outside"));

        verify(baseTable).substitute(0, 1, "CONTENT1");
        verify(baseTable).substitute(1, 2, "CONTENT2");
    }

    @Test
    void should_map_to_delegate_when_updateContent() {
        doNothing().when(baseTable).updateContent(anyInt(), anyInt(), eq(TEST_RESULT));
        doNothing().when(baseTable).updateContent(anyInt(), anyInt(), eq(EXCEPTION_RESULT));

        final SubTable subTable = subTable(0, 2, 1, 2);

        subTable.updateContent(0, 0, TEST_RESULT);
        subTable.updateContent(1, 1, EXCEPTION_RESULT);

        verify(baseTable).updateContent(0, 1, TEST_RESULT);
        verify(baseTable).updateContent(1, 2, EXCEPTION_RESULT);
    }

    @Test
    void should_simulate_blank_cell_when_subtable_with_other_than_first_missing_row() {
        baseTable = spy(StringTable.from("|a0|a1|a2   |",
                                         "|b0         |",
                                         "|c0|c1|c2|c3|"));

        final SubTable subTable = subTable(2, 3, 0, 2);

        assertThat(subTable.getRowCount(), is(3));
        assertThat(subTable.getColumnCountInRow(1), is(1));
        assertThat(subTable.getCellContents(0, 1), is(""));
        assertThrows(RuntimeException.class, () -> subTable.getCellContents(1, 1));
        assertThat(StringTable.from(subTable), is(StringTable.from("|a2   |",
                                                                   "|     |",
                                                                   "|c2|c3|")));

        subTable.substitute(0, 1, "IGNORED");
        assertThat(subTable.getCellContents(0, 1), is(""));
        verify(baseTable, never()).substitute(anyInt(), anyInt(), anyString());
        assertThrows(RuntimeException.class, () -> subTable.substitute(1, 1, "outside"));

        subTable.updateContent(0, 1, TEST_RESULT);
        verify(baseTable, never()).updateContent(anyInt(), anyInt(), eq(TEST_RESULT));
        assertThrows(RuntimeException.class, () -> subTable.updateContent(1, 1, TEST_RESULT));

        subTable.updateContent(0, 1, EXCEPTION_RESULT);
        verify(baseTable, never()).updateContent(anyInt(), anyInt(), eq(EXCEPTION_RESULT));
        assertThrows(RuntimeException.class, () -> subTable.updateContent(1, 1, EXCEPTION_RESULT));
    }

}