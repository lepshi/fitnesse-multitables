package net.lepshi.fitnesse.mutitables.plugin.common;

import fitnesse.testsystems.Assertion;
import org.junit.jupiter.api.Test;

import static com.google.common.base.Joiner.on;
import static java.util.Arrays.asList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class AssertionsFormatterTest {

    @Test
    void should_format_assertions_list() {

        final String result = AssertionsFormatter.formatAssertions("Header: ",
                                                                   asList(mockAssertion("assert 1"),
                                                                          mockAssertion("assert 2"),
                                                                          mockAssertion("assert 3")));

        assertThat(result, is(on('\n').join("Header: ",
                                            "[0]",
                                            "assert 1",
                                            "[1]",
                                            "assert 2",
                                            "[2]",
                                            "assert 3")));
    }

    private Assertion mockAssertion(String name) {
        Assertion assertion = mock(Assertion.class);

        when(assertion.toString()).thenReturn(name);

        return assertion;
    }
}