package net.lepshi.fitnesse.mutitables.plugin.common;

import fitnesse.testsystems.slim.Table;
import org.junit.jupiter.api.Test;

import static com.google.common.base.Joiner.on;
import static java.util.Arrays.asList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class StringTableTest {

    private final StringTable table_empty = StringTable.from("||");

    private final StringTable table_1x1 = StringTable.from("  |a |");

    private final StringTable table_3x3 = StringTable.from("|x|o| |",
                                                           "|o|x| |",
                                                           "| |o|x|");

    private final StringTable table_varCols = StringTable.from("|  long       |",
                                                               "|short0|short1|");

    @Test
    void should_getCellContents() {
        assertThat(table_empty.getCellContents(0, 0), is(""));
        assertThrows(RuntimeException.class, () -> table_empty.getCellContents(1, 0));
        assertThrows(RuntimeException.class, () -> table_empty.getCellContents(0, 1));

        assertThat(table_1x1.getCellContents(0, 0), is("a"));

        assertThat(table_3x3.getCellContents(0, 0), is("x"));
        assertThat(table_3x3.getCellContents(1, 0), is("o"));
        assertThat(table_3x3.getCellContents(2, 0), is(""));
        assertThat(table_3x3.getCellContents(0, 1), is("o"));
        assertThat(table_3x3.getCellContents(1, 1), is("x"));
        assertThat(table_3x3.getCellContents(2, 1), is(""));
        assertThat(table_3x3.getCellContents(0, 2), is(""));
        assertThat(table_3x3.getCellContents(1, 2), is("o"));
        assertThat(table_3x3.getCellContents(2, 2), is("x"));

        assertThat(table_varCols.getCellContents(0, 0), is("long"));
        assertThat(table_varCols.getCellContents(0, 1), is("short0"));
        assertThat(table_varCols.getCellContents(1, 1), is("short1"));
        assertThrows(RuntimeException.class, () -> table_varCols.getCellContents(1, 0));
        assertThrows(RuntimeException.class, () -> table_varCols.getCellContents(2, 0));
    }

    @Test
    void should_getRowCount() {
        assertThat(table_empty.getRowCount(), is(1));
        assertThat(table_1x1.getRowCount(), is(1));
        assertThat(table_3x3.getRowCount(), is(3));
        assertThat(table_varCols.getRowCount(), is(2));
    }

    @Test
    void should_getColumnCountInRow() {
        assertThat(table_empty.getColumnCountInRow(0), is(1));
        assertThrows(RuntimeException.class, () -> table_empty.getColumnCountInRow(1));

        assertThat(table_1x1.getColumnCountInRow(0), is(1));

        assertThat(table_3x3.getColumnCountInRow(0), is(3));
        assertThat(table_3x3.getColumnCountInRow(1), is(3));
        assertThat(table_3x3.getColumnCountInRow(2), is(3));

        assertThat(table_varCols.getColumnCountInRow(0), is(1));
        assertThat(table_varCols.getColumnCountInRow(1), is(2));
        assertThrows(RuntimeException.class, () -> table_empty.getColumnCountInRow(2));
    }

    @Test
    void should_substitute() {
        table_3x3.substitute(2, 0, "a");
        table_3x3.substitute(1, 1, "b");
        table_3x3.substitute(0, 2, "c");

        assertThat(table_3x3.getCellContents(2, 0), is("a"));
        assertThat(table_3x3.getCellContents(1, 1), is("b"));
        assertThat(table_3x3.getCellContents(0, 2), is("c"));
    }

    @Test
    void should_convert_toString() {
        table_3x3.substitute(0, 2, "c");

        assertThat(table_3x3.toString(), is(on("\n").join("|x|o||",
                                                          "|o|x||",
                                                          "|c|o|x|")));
        assertThat(table_varCols.toString(), is(on("\n").join("|long|",
                                                              "|short0|short1|")));
    }

    @Test
    void should_be_equal_when_input_format_differs() {
        StringTable table1 = StringTable.from("|1|2|",
                                              "| 3 |");
        StringTable table2 = StringTable.from("| 1| 2 |",
                                              "|     3|");

        assertThat(table1, is(equalTo(table2)));
    }

    @Test
    void should_not_be_equal_when_content_differs() {
        StringTable table1 = StringTable.from("|1|2|",
                                              "| 3 |");
        StringTable table2 = StringTable.from("|i|2|",
                                              "| 3 |");

        assertThat(table1, is(not(equalTo(table2))));
    }

    @Test
    void should_addRow() {
        assertThat(table_varCols.addRow(asList("a", "b", "c", "d", "e")), is(3));

        assertThat(table_varCols, is(equalTo(StringTable.from("|long         |",
                                                              "|short0|short1|",
                                                              "|a |b |c |d |e|"))));
    }

    @Test
    void should_construct_from_other_table() {
        final Table other = mock(Table.class);
        when(other.getRowCount()).thenReturn(2);
        when(other.getColumnCountInRow(0)).thenReturn(1);
        when(other.getColumnCountInRow(1)).thenReturn(2);
        when(other.getCellContents(0, 0)).thenReturn("cell[0,0]");
        when(other.getCellContents(0, 1)).thenReturn("cell[0,1]");
        when(other.getCellContents(1, 1)).thenReturn("cell[1,1]");

        assertThat(StringTable.from(other), is(StringTable.from("| cell[0,0]             |",
                                                                "| cell[0,1] | cell[1,1] |")));
    }

    @Test
    void should_construct_empty() {
        StringTable expected = StringTable.from("| | | |",
                                                "| | | |");

        assertThat(StringTable.empty(3, 2), is(equalTo(expected)));
    }

}