package net.lepshi.fitnesse.mutitables.plugin.data;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

class NodeNameBuilderTest {

    private ObjectStructureNodes.CompositeNode root = new ObjectStructureNodes.CompositeNode(null);


    @Test
    void should_build_name_when_nodes_ROOT_simple() {
        givenNode("fieldA");

        assertName(child(0), "ROOT");
        assertName(child(1), "fieldA");
    }

    @Test
    void should_build_name_when_ROOT_composite_simple() {
        givenNode("nested{fieldB}");

        assertName(child(0), "ROOT");
        assertName(child(1), "nested");
        assertName(child(2), "nested{fieldB}");
    }

    @Test
    void should_build_name_when_ROOT_array_simple() {
        givenNode("elems[]");

        assertName(child(0), "ROOT");
        assertName(child(1), "elems");
        assertName(child(2), "elems[]");
    }

    @Test
    void should_build_name_when_ROOT_array_composite_simple() {
        givenNode("elems[]{fieldC}");

        assertName(child(0), "ROOT");
        assertName(child(1), "elems");
        assertName(child(2), "elems[]");
        assertName(child(3), "elems[]{fieldC}");
    }

    @Test
    void should_build_name_when_complex() {
        givenNode("a[]{b{c[]{d[]}}}");

        assertName(child(0), "ROOT");
        assertName(child(1), "a");
        assertName(child(2), "a[]");
        assertName(child(3), "a[]{b}");
        assertName(child(4), "a[]{b{c}}");
        assertName(child(5), "a[]{b{c[]}}");
        assertName(child(6), "a[]{b{c[]{d}}}");
        assertName(child(7), "a[]{b{c[]{d[]}}}");
    }


    private void givenNode(String nodeSpec) {
        new ObjectStructureBuilder(null).buildNodeFromRoot(root, nodeSpec);
    }

    private void assertName(ObjectStructureNodes.Node node, String expectedName) {
        String nodeName = new NodeNameBuilder(node).buildName();
        assertThat(nodeName, is(expectedName));
    }

    private ObjectStructureNodes.Node child(int level) {
        return child(root, level);
    }

    private ObjectStructureNodes.Node child(ObjectStructureNodes.Node node, int level) {
        if (level == 0) {
            return node;
        }
        ObjectStructureNodes.Node firstChild = ((ObjectStructureNodes.CompositeNode) node).children.values()
                                                                                                   .iterator()
                                                                                                   .next();
        return child(firstChild, level - 1);
    }
}