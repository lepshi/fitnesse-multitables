package net.lepshi.fitnesse.mutitables.plugin.common;

import fitnesse.testsystems.slim.Table;
import fitnesse.testsystems.slim.results.SlimExceptionResult;
import fitnesse.testsystems.slim.results.SlimTestResult;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.anyInt;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

class CompositeTableTest {

    @Spy
    private Table tableTitle  = StringTable.from("|title|");
    @Spy
    private Table tableHeader = StringTable.from("|header0|header1|");
    @Spy
    private Table tableData   = StringTable.from("|a0|a1|a2|",
                                                 "|b0      |",
                                                 "|c0|c1   |");

    private CompositeTable compositeTable;

    @BeforeEach
    void init() {
        MockitoAnnotations.initMocks(this);
        compositeTable = new CompositeTable(tableTitle, tableHeader, tableData);
    }


    @Test
    void should_fail_to_construct_when_no_source_table_given() {

        assertThrows(IllegalArgumentException.class, CompositeTable::new);
    }

    @Test
    void should_delegate_to_first_when_isTearDown() {
        doReturn(true).when(tableTitle).isTearDown();

        assertThat(compositeTable.isTearDown(), is(true));

        verify(tableTitle).isTearDown();
        verify(tableHeader, never()).isTearDown();
        verify(tableData, never()).isTearDown();
    }

    @Test
    void should_map_to_delegate_when_getCellContents() {
        assertThat(compositeTable.getCellContents(0, 0), is("title"));
        assertThrows(RuntimeException.class, () -> compositeTable.getCellContents(1, 0));

        assertThat(compositeTable.getCellContents(0, 1), is("header0"));
        assertThat(compositeTable.getCellContents(1, 1), is("header1"));
        assertThrows(RuntimeException.class, () -> compositeTable.getCellContents(2, 1));

        assertThat(compositeTable.getCellContents(0, 2), is("a0"));
        assertThat(compositeTable.getCellContents(1, 2), is("a1"));
        assertThat(compositeTable.getCellContents(2, 2), is("a2"));
        assertThrows(RuntimeException.class, () -> compositeTable.getCellContents(3, 2));
        assertThat(compositeTable.getCellContents(0, 3), is("b0"));
        assertThrows(RuntimeException.class, () -> compositeTable.getCellContents(1, 3));
        assertThat(compositeTable.getCellContents(0, 4), is("c0"));
        assertThat(compositeTable.getCellContents(1, 4), is("c1"));
        assertThrows(RuntimeException.class, () -> compositeTable.getCellContents(2, 4));
    }

    @Test
    void should_report_composite_getRowCount() {
        assertThat(compositeTable.getRowCount(), is(5));
    }

    @Test
    void should_report_getColumnCountInRow_from_component() {
        assertThat(compositeTable.getColumnCountInRow(0), is(1));
        assertThat(compositeTable.getColumnCountInRow(1), is(2));
        assertThat(compositeTable.getColumnCountInRow(2), is(3));
        assertThat(compositeTable.getColumnCountInRow(3), is(1));
        assertThat(compositeTable.getColumnCountInRow(4), is(2));
        assertThrows(IllegalArgumentException.class, () -> compositeTable.getColumnCountInRow(5));
    }

    @Test
    void should_substitute_on_component() {
        compositeTable.substitute(0, 0, "s1");
        compositeTable.substitute(1, 1, "s2");
        compositeTable.substitute(2, 2, "s3");
        compositeTable.substitute(1, 4, "s4");
        assertThrows(IllegalArgumentException.class, () -> compositeTable.substitute(0, 5, "outside"));

        verify(tableTitle).substitute(0, 0, "s1");
        verify(tableHeader).substitute(1, 0, "s2");
        verify(tableData).substitute(2, 0, "s3");
        verify(tableData).substitute(1, 2, "s4");

        assertThat(StringTable.from(tableTitle), is(StringTable.from(" |s1        |")));
        assertThat(StringTable.from(tableHeader), is(StringTable.from("|header0|s2|")));
        assertThat(StringTable.from(tableData), is(StringTable.from("  |a0|a1|s3  |",
                                                                    "  |b0        |",
                                                                    "  |c0|s4     |")));
    }

    @Test
    void should_updateContent_on_components() {
        final SlimTestResult TEST_RESULT = mock(SlimTestResult.class);
        final SlimExceptionResult EXCEPTION_RESULT = mock(SlimExceptionResult.class);

        doNothing().when(tableTitle).updateContent(anyInt(), anyInt(), eq(TEST_RESULT));
        doNothing().when(tableTitle).updateContent(anyInt(), anyInt(), eq(EXCEPTION_RESULT));
        doNothing().when(tableData).updateContent(anyInt(), anyInt(), eq(TEST_RESULT));
        doNothing().when(tableData).updateContent(anyInt(), anyInt(), eq(EXCEPTION_RESULT));

        compositeTable.updateContent(0, 0, TEST_RESULT);
        compositeTable.updateContent(2, 2, EXCEPTION_RESULT);
        assertThrows(IllegalArgumentException.class, () -> compositeTable.updateContent(0, 5, TEST_RESULT));

        verify(tableTitle).updateContent(0, 0, TEST_RESULT);
        verify(tableData).updateContent(2, 0, EXCEPTION_RESULT);
    }

}