package net.lepshi.fitnesse.mutitables.plugin.common;

import fitnesse.testsystems.ExecutionResult;
import fitnesse.testsystems.slim.Table;
import fitnesse.testsystems.slim.results.SlimExceptionResult;
import fitnesse.testsystems.slim.results.SlimTestResult;
import net.lepshi.fitnesse.mutitables.plugin.data.Rows;
import org.apache.commons.lang3.math.NumberUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatchers;

import static fitnesse.testsystems.ExecutionResult.ERROR;
import static fitnesse.testsystems.ExecutionResult.FAIL;
import static fitnesse.testsystems.ExecutionResult.IGNORE;
import static fitnesse.testsystems.ExecutionResult.PASS;
import static java.lang.String.format;
import static net.lepshi.fitnesse.mutitables.plugin.common.Cell.cell;
import static net.lepshi.fitnesse.mutitables.plugin.common.TableCells.cellsOf;
import static net.lepshi.fitnesse.mutitables.plugin.common.TableDataHighlighterFactory.ADJUSTMENT_FOR_HIGHLIGHTING;
import static net.lepshi.fitnesse.mutitables.plugin.common.TableDataHighlighterFactory.NONE_EXEC_RESULT;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyInt;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

class TableDataHighlighterFactoryTest {

    private static final String ERR_MSG = "error msg";

    private Table table = spy(StringTable.from("|a0      |",
                                               "|b0|b1|b2|",
                                               "|c0|c1   |"));

    private TableDataHighlighterFactory factory = new TableDataHighlighterFactory(table);

    private ArgumentCaptor<SlimTestResult>      testResultCaptor      = ArgumentCaptor.forClass(SlimTestResult.class);
    private ArgumentCaptor<SlimExceptionResult> exceptionResultCaptor = ArgumentCaptor.forClass(SlimExceptionResult.class);


    @BeforeEach
    void init() {
        doNothing().when(table).updateContent(anyInt(), anyInt(), any(SlimTestResult.class));
        doNothing().when(table).updateContent(anyInt(), anyInt(), any(SlimExceptionResult.class));
    }


    @Test
    void should_adjust_ExecutionResult_when_non_highlightable() {
        assertThat(ADJUSTMENT_FOR_HIGHLIGHTING.apply(ERROR), is(FAIL));
        assertThat(ADJUSTMENT_FOR_HIGHLIGHTING.apply(FAIL), is(FAIL));
        assertThat(ADJUSTMENT_FOR_HIGHLIGHTING.apply(IGNORE), is(IGNORE));
        assertThat(ADJUSTMENT_FOR_HIGHLIGHTING.apply(PASS), is(PASS));
        assertThat(ADJUSTMENT_FOR_HIGHLIGHTING.apply(null), is(nullValue()));
    }

    @Test
    void should_highlight_cell_using_FAIL() {

        factory.cell(FAIL, 1, 2).highlight();

        assertTestResult(StringTable.from("|0|0|0|",
                                          "|0|0|0|",
                                          "|0|1|0|"),
                         FAIL,
                         null);

        assertExceptionResultCount(0);
    }

    @Test
    void should_highlight_cell_using_FAIL_with_message() {

        factory.cell(FAIL, 1, 2, ERR_MSG).highlight();

        assertTestResult(StringTable.from("|0|0|0|",
                                          "|0|0|0|",
                                          "|0|1|0|"),
                         FAIL,
                         ERR_MSG);

        assertExceptionResultCount(0);
    }

    @Test
    void should_highlight_cell_using_IGNORE() {

        factory.cell(IGNORE, 2, 1).highlight();

        assertTestResult(StringTable.from("|0|0|0|",
                                          "|0|0|1|",
                                          "|0|0|0|"),
                         IGNORE,
                         null);

        assertExceptionResultCount(0);
    }

    @Test
    void should_highlight_cell_using_PASS() {

        factory.cell(PASS, 1, 2).highlight();

        assertTestResult(StringTable.from("|0|0|0|",
                                          "|0|0|0|",
                                          "|0|1|0|"),
                         PASS,
                         null);

        assertExceptionResultCount(0);
    }

    @Test
    void should_NOT_highlight_cell_using_NONE() {

        factory.cell(NONE_EXEC_RESULT, 1, 2).highlight();

        assertTestResult(StringTable.from("|0|0|0|",
                                          "|0|0|0|",
                                          "|0|0|0|"),
                         null,
                         null);

        assertExceptionResultCount(0);
    }

    @Test
    void should_highlight_cell_using_PLAIN_with_message() {

        factory.cell(NONE_EXEC_RESULT, 1, 2, ERR_MSG).highlight();

        assertTestResult(StringTable.from("|0|0|0|",
                                          "|0|0|0|",
                                          "|0|1|0|"),
                         null,
                         ERR_MSG);

        assertExceptionResultCount(0);
    }


    @Test
    void should_highlight_row_using_FAIL() {

        factory.row(FAIL, 2).highlight();

        assertTestResult(StringTable.from("|0|0|0|",
                                          "|0|0|0|",
                                          "|1|1|0|"),
                         FAIL,
                         null);

        assertExceptionResultCount(0);
    }

    @Test
    void should_highlight_row_using_FAIL_with_message() {

        factory.row(FAIL, 2, ERR_MSG).highlight();

        assertTestResult(StringTable.from("|0|0|0|",
                                          "|0|0|0|",
                                          "|1|1|0|"),
                         FAIL,
                         null);

        assertExceptionResult(cell(0, 2), ERR_MSG);
    }

    @Test
    void should_highlight_row_using_IGNORE() {

        factory.row(IGNORE, 2).highlight();

        assertTestResult(StringTable.from("|0|0|0|",
                                          "|0|0|0|",
                                          "|1|1|0|"),
                         IGNORE,
                         null);

        assertExceptionResultCount(0);
    }

    @Test
    void should_highlight_row_using_PASS_when_row_PASS() {

        factory.row(PASS, 1).highlight();

        assertTestResult(StringTable.from("|0|0|0|",
                                          "|1|1|1|",
                                          "|0|0|0|"),
                         PASS,
                         null);

        assertExceptionResultCount(0);
    }

    @Test
    void should_highlight_row_using_PLAIN_with_message() {

        factory.row(NONE_EXEC_RESULT, 1, ERR_MSG).highlight();

        assertTestResult(StringTable.from("|0|0|0|",
                                          "|0|0|0|",
                                          "|0|0|0|"),
                         null,
                         null);

        assertExceptionResult(cell(0, 1), ERR_MSG);
    }


    @Test
    void should_highlight_table_using_FAIL() {

        factory.table(FAIL).highlight();

        assertTestResult(StringTable.from("|1|0|0|",
                                          "|1|1|1|",
                                          "|1|1|0|"),
                         FAIL,
                         null);

        assertExceptionResultCount(0);
    }

    @Test
    void should_highlight_table_using_IGNORE_with_message() {

        factory.table(IGNORE, ERR_MSG).highlight();

        assertTestResult(StringTable.from("|1|0|0|",
                                          "|1|1|1|",
                                          "|1|1|0|"),
                         IGNORE,
                         null);

        assertExceptionResult(cell(0, 2), ERR_MSG);
    }

    @Test
    void should_highlight_table_using_PASS() {

        factory.table(PASS).highlight();

        assertTestResult(StringTable.from("|1|0|0|",
                                          "|1|1|1|",
                                          "|1|1|0|"),
                         PASS,
                         null);

        assertExceptionResultCount(0);
    }

    @Test
    void should_highlight_table_using_PLAIN_with_message() {

        factory.table(NONE_EXEC_RESULT, ERR_MSG).highlight();

        assertTestResult(StringTable.from("|0|0|0|",
                                          "|0|0|0|",
                                          "|0|0|0|"),
                         null,
                         null);

        assertExceptionResult(cell(0, 2), ERR_MSG);
    }

    @Test
    void should_highlight_table_rows_using_FAIL() {

        factory.rows(FAIL, Rows.range(1, 2)).highlight();

        assertTestResult(StringTable.from("|0|0|0|",
                                          "|1|1|1|",
                                          "|1|1|0|"),
                         FAIL,
                         null);

        assertExceptionResultCount(0);
    }

    @Test
    void should_highlight_table_rows_using_IGNORE_with_message() {

        factory.rows(IGNORE, Rows.range(0, 1), ERR_MSG).highlight();

        assertTestResult(StringTable.from("|1|0|0|",
                                          "|1|1|1|",
                                          "|0|0|0|"),
                         IGNORE,
                         null);

        assertExceptionResult(cell(0, 1), ERR_MSG);
    }

    @Test
    void should_highlight_table_cell_using_attached_label_message() {

        factory.cellLabel(1, 1, ERR_MSG).highlight();

        assertTestResult(StringTable.from("|0|0|0|",
                                          "|0|0|0|",
                                          "|0|0|0|"),
                         null,
                         null);

        assertExceptionResult(cell(1, 1), format("message:<<%s>>", ERR_MSG));
    }


    private void assertTestResult(StringTable updatedCells,
                                  ExecutionResult executionResult,
                                  String overriddenText) {

        for (Cell updatedCell : cellsOf(updatedCells)) {
            String cellContents = updatedCells.getCellContents(updatedCell.col, updatedCell.row);
            if (NumberUtils.isParsable(cellContents)) {
                int timesUpdated = Integer.parseInt(cellContents);

                verify(table, times(timesUpdated)).updateContent(ArgumentMatchers.eq(updatedCell.col), ArgumentMatchers.eq(updatedCell.row), testResultCaptor.capture());
                if (timesUpdated > 0) {
                    final SlimTestResult testResult = testResultCaptor.getValue();
                    assertThat(testResult.getExecutionResult(), is(executionResult));
                    assertThat(testResult.getMessage(), is(overriddenText));
                }
            }
        }
    }

    private void assertExceptionResult(Cell cell, String exceptionValue) {
        assertExceptionResultCount(1);
        verify(table).updateContent(ArgumentMatchers.eq(cell.col),
                                    ArgumentMatchers.eq(cell.row),
                                    exceptionResultCaptor.capture());
        final SlimExceptionResult exceptionResult = exceptionResultCaptor.getValue();
        assertThat(exceptionResult.getException(), is(exceptionValue));
    }

    private void assertExceptionResultCount(int count) {
        verify(table, times(count)).updateContent(anyInt(),
                                                  anyInt(),
                                                  any(SlimExceptionResult.class));
    }
}