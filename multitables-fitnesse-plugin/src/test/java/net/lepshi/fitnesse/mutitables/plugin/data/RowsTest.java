package net.lepshi.fitnesse.mutitables.plugin.data;

import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.is;

class RowsTest {

    @Test
    void should_provide_first_and_last_when_range() {
        Rows rows = Rows.range(2, 4);
        assertThat(rows.first, is(2));
        assertThat(rows.last, is(4));
    }

    @Test
    void should_provide_same_first_and_last_when_single() {
        Rows rows = Rows.single(7);
        assertThat(rows.first, is(7));
        assertThat(rows.last, is(7));
    }

    @Test
    void should_provide_count_when_positive() {
        Rows rows = Rows.range(2, 5);
        assertThat(rows.count(), is(4));
    }

    @Test
    void should_provide_count_when_single() {
        Rows rows = Rows.single(3);
        assertThat(rows.count(), is(1));
    }

    @Test
    void should_provide_count_zero_when_no_rows() {
        Rows rows = Rows.range(9, 6);
        assertThat(rows.count(), is(0));
    }

    @Test
    void should_stream_rows_as_IntStream() {
        Rows rows = Rows.range(0, 3);
        assertThat(toIntList(rows.stream()), contains(0, 1, 2, 3));
    }

    @Test
    void should_adjust_when_first_row_excluded() {
        Rows rows = Rows.range(2, 4);
        assertThat(rows.count(), is(3));

        rows = rows.exceptFirst();

        assertThat(rows.first, is(3));
        assertThat(rows.last, is(4));
        assertThat(rows.count(), is(2));
    }

    @Test
    void should_adjust_when_first_row_excluded_from_single_row() {
        Rows rows = Rows.single(66);

        rows = rows.exceptFirst();

        assertThat(rows.count(), is(0));
    }


    private List<Integer> toIntList(IntStream rowsStream) {
        return rowsStream.boxed()
                         .collect(toList());
    }
}