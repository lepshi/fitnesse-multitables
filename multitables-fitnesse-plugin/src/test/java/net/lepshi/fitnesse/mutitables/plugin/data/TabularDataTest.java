package net.lepshi.fitnesse.mutitables.plugin.data;

import com.google.gson.Gson;
import fitnesse.testsystems.slim.Table;
import net.lepshi.fitnesse.mutitables.plugin.common.StringTable;
import lombok.Getter;
import lombok.Setter;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Set;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

class TabularDataTest {

    private final Gson gson = new Gson();

    private Table dataTable;


    @Getter
    @Setter
    private static class SimplePojo {
        private String  stringField;
        private int     intField;
        private long    longField;
        private float   floatField;
        private double  doubleField;
        private boolean booleanField;
    }

    @Test
    void should_parse_simple_pojo() {
        givenDataTable(StringTable.from("|stringField|intField|longField|floatField|doubleField|booleanField|",
                                        "|abc        |123     |4567     |6.32      |9.87       |true        |"));

        SimplePojo obj = dataToObject(SimplePojo.class);

        assertThat(obj.stringField, is("abc"));
        assertThat(obj.intField, is(123));
        assertThat(obj.longField, is(4567L));
        assertThat(obj.floatField, is(6.32f));
        assertThat(obj.doubleField, is(9.87));
        assertThat(obj.booleanField, is(true));
    }


    @Getter
    @Setter
    private static class Inner {
        private String stringField;
        private int    intField;
    }

    @Getter
    @Setter
    private static class Outer {
        private Inner inner;
        private float simple;
    }

    @Test
    void should_parse_nested_structure() {
        givenDataTable(StringTable.from("|inner{stringField}|simple|inner{intField}|",
                                        "|abc               |-1.23 |123            |"));

        Outer obj = dataToObject(Outer.class);

        assertThat(obj.simple, is(-1.23f));
        assertThat(obj.inner, is(notNullValue()));
        assertThat(obj.inner.stringField, is("abc"));
        assertThat(obj.inner.intField, is(123));
    }

    @Test
    void should_parse_empty_structure() {
        givenDataTable(StringTable.from("|inner{stringField}|simple|inner{intField}|",
                                        "|                  |      |               |",
                                        "|                  |      |               |"));

        Outer obj = dataToObject(Outer.class);

        assertThat(obj.simple, is(0.0f));
        assertThat(obj.inner, is(nullValue()));
    }

    @Test
    void should_parse_empty_nested_value() {
        givenDataTable(StringTable.from("|simple|inner{intField}|",
                                        "|1.1   |               |",
                                        "|      |               |"));

        Outer obj = dataToObject(Outer.class);

        assertThat(obj.simple, is(1.1f));
        assertThat(obj.inner.intField, is(0));
        assertThat(obj.inner.stringField, is(nullValue()));
    }

    @Test
    void should_fail_when_multiple_values_for_non_array_field_in_root() {
        givenDataTable(StringTable.from("|simple|",
                                        "|1.1   |",
                                        "|2.2   |"));

        assertThat(assertThrows(RuntimeException.class, () -> dataToObject(Outer.class))
                           .getMessage(), allOf(containsString("Unexpected data on other than base row"),
                                                not(containsString("(while base row is empty)")),
                                                containsString("node('simple')=value('2.2')")));
    }

    @Test
    void should_fail_when_multiple_values_for_non_array_nested_field() {
        givenDataTable(StringTable.from("|simple|inner{intField}|",
                                        "|3.3   |1              |",
                                        "|      |2              |"));

        assertThat(assertThrows(RuntimeException.class, () -> dataToObject(Outer.class))
                           .getMessage(), allOf(containsString("Unexpected data on other than base row"),
                                                not(containsString("(while base row is empty)")),
                                                containsString("node('inner{intField}')=value('2')")));
    }


    @Getter
    @Setter
    private static class PrimitiveArraysHolder {
        private int       simpleInt;
        private String[]  strings;
        private Integer[] ints;
        private Double[]  doubles;
        private Boolean[] booleans;
    }

    @Test
    void should_parse_object_with_simple_arrays() {
        givenDataTable(StringTable.from("|simpleInt|doubles[]|strings[]|ints[]|booleans[]|",
                                        "|101      |1.01     | a a     |     1|true      |",
                                        "|         |2.02     | b b     |     2|false     |",
                                        "|         |         | c c     |     3|          |"));

        PrimitiveArraysHolder obj = dataToObject(PrimitiveArraysHolder.class);

        assertThat(obj.simpleInt, is(101));
        assertThat(obj.doubles, arrayContaining(1.01, 2.02));
        assertThat(obj.strings, arrayContaining("a a", "b b", "c c"));
        assertThat(obj.ints, arrayContaining(1, 2, 3));
        assertThat(obj.booleans, arrayContaining(true, false));
    }


    @Getter
    @Setter
    private static class InnerArraysHolder {
        private String                str;
        private PrimitiveArraysHolder inner;
    }

    @Test
    void should_parse_object_with_nested_simple_arrays() {
        givenDataTable(StringTable.from(
                "|inner{simpleInt}|inner{ints[]}|inner{strings[]}|inner{booleans[]}|str|",
                "|        12      |       9     |        a       |                 | s |",
                "|                |      10     |        b       |                 |   |",
                "|                |      11     |                |                 |   |"));

        InnerArraysHolder obj = dataToObject(InnerArraysHolder.class);

        assertThat(obj.str, is("s"));
        assertThat(obj.inner.simpleInt, is(12));
        assertThat(obj.inner.strings, arrayContaining("a", "b"));
        assertThat(obj.inner.ints, arrayContaining(9, 10, 11));
        assertThat(obj.inner.doubles, is(nullValue()));
        assertThat(obj.inner.booleans, is(arrayWithSize(0)));
    }

    @Test
    void should_fail_when_gap_between_array_elements() {
        givenDataTable(StringTable.from("|strings[]|",
                                        "|elem1    |",
                                        "|         |",
                                        "|elem2    |"));

        assertThat(assertThrows(RuntimeException.class, () -> dataToObject(PrimitiveArraysHolder.class))
                           .getMessage(), containsString("Node('strings[]'): No gap is allowed before simple (non-composite) array value 'elem2'"));
    }

    @Getter
    @Setter
    private static class LinkedArrayNode {
        private String          value;
        private LinkedArrayNode next;
    }

    @Test
    void should_parse_deeply_nested_values() {
        givenDataTable(StringTable.from("|value |next{value}|next{next{value}}|next{next{next{value}}}|",
                                        "|node1 |node2      |node3            |node4                  |"));

        LinkedArrayNode obj = dataToObject(LinkedArrayNode.class);

        assertThat(obj.value, is("node1"));
        assertThat(obj.next.value, is("node2"));
        assertThat(obj.next.next.value, is("node3"));
        assertThat(obj.next.next.next.value, is("node4"));
        assertThat(obj.next.next.next.next, is(nullValue()));
    }

    @Getter
    @Setter
    private static class RecursiveArrayNode {
        private String               id;
        private String[]             strings;
        private Integer[]            ints;
        private RecursiveArrayNode   next;
        private RecursiveArrayNode[] children;
    }

    @Test
    void should_parse_nested_simple_array() {
        givenDataTable(StringTable.from("|id|next{strings[]}|",
                                        "|o1|s1.1           |",
                                        "|  |s1.2           |",
                                        "|  |s1.3           |"));

        RecursiveArrayNode obj = dataToObject(RecursiveArrayNode.class);

        assertThat(obj.id, is("o1"));
        assertThat(obj.strings, is(nullValue()));
        assertThat(obj.ints, is(nullValue()));
        assertThat(obj.next.strings, arrayContaining("s1.1", "s1.2", "s1.3"));
        assertThat(obj.next.ints, is(nullValue()));
        assertThat(obj.next.next, is(nullValue()));
    }

    @Test
    void should_parse_mix_of_nested_objects_and_simple_arrays() {
        givenDataTable(StringTable.from("|id|strings[]|next{id}|next{ints[]}|next{next{id}}|next{next{strings[]}}|",
                                        "|o1|s1       |o1.1    |1           |o1.1.1        |s1.1.1               |",
                                        "|  |s2       |        |2           |              |s1.1.2               |",
                                        "|  |         |        |            |              |s1.1.3               |"));

        RecursiveArrayNode obj = dataToObject(RecursiveArrayNode.class);

        assertThat(obj.id, is("o1"));
        assertThat(obj.strings, arrayContaining("s1", "s2"));
        assertThat(obj.ints, is(nullValue()));
        assertThat(obj.next.id, is("o1.1"));
        assertThat(obj.next.strings, is(nullValue()));
        assertThat(obj.next.ints, arrayContaining(1, 2));
        assertThat(obj.next.next.id, is("o1.1.1"));
        assertThat(obj.next.next.ints, is(nullValue()));
        assertThat(obj.next.next.strings, arrayContaining("s1.1.1", "s1.1.2", "s1.1.3"));
    }

    @Test
    void should_parse_nested_objects_and_simple_arrays_when_no_simple_fields_defined() {
        givenDataTable(StringTable.from("|strings[]|next{strings[]}|next{next{strings[]}}|",
                                        "|s1       |s1.1           |s1.1.1               |",
                                        "|s2       |s1.2           |s1.1.2               |",
                                        "|         |s1.3           |s1.1.3               |"));

        RecursiveArrayNode obj = dataToObject(RecursiveArrayNode.class);

        assertThat(obj.id, is(nullValue()));
        assertThat(obj.strings, arrayContaining("s1", "s2"));
        assertThat(obj.next.id, is(nullValue()));
        assertThat(obj.next.strings, arrayContaining("s1.1", "s1.2", "s1.3"));
        assertThat(obj.next.next.id, is(nullValue()));
        assertThat(obj.next.next.strings, arrayContaining("s1.1.1", "s1.1.2", "s1.1.3"));
    }

    @Test
    void should_parse_object_array() {
        givenDataTable(StringTable.from("|id|children[]{id}|",
                                        "|o1|o1.1          |",
                                        "|  |o1.2          |",
                                        "|  |o1.3          |"));

        RecursiveArrayNode obj = dataToObject(RecursiveArrayNode.class);

        assertThat(obj.id, is("o1"));
        assertThat(obj.children.length, is(3));
        assertThat(obj.children[0].id, is("o1.1"));
        assertThat(obj.children[0].strings, is(nullValue()));
        assertThat(obj.children[1].id, is("o1.2"));
        assertThat(obj.children[1].strings, is(nullValue()));
        assertThat(obj.children[2].id, is("o1.3"));
        assertThat(obj.children[2].strings, is(nullValue()));
    }

    @Test
    void should_parse_two_children_with_array() {
        givenDataTable(StringTable.from("|id  |children[]{id}|children[]{strings[]}|",
                                        "|base|child1_base   |bound_to_child1      |",
                                        "|    |              |bound_to_child1      |",
                                        "|    |child2_base   |bound_to_child2      |",
                                        "|    |              |bound_to_child2      |"));

        RecursiveArrayNode obj = dataToObject(RecursiveArrayNode.class);

        assertThat(obj.id, is("base"));
        assertThat(obj.children.length, is(2));
        assertThat(obj.children[0].id, is("child1_base"));
        assertThat(obj.children[0].strings, arrayContaining("bound_to_child1", "bound_to_child1"));
        assertThat(obj.children[1].id, is("child2_base"));
        assertThat(obj.children[1].strings, arrayContaining("bound_to_child2", "bound_to_child2"));
    }

    @Test
    void should_parse_single_child_with_array_when_blank_children_base_data() {
        givenDataTable(StringTable.from("|id  |children[]{id}|children[]{strings[]}|",
                                        "|base|              |bound_to_base        |",
                                        "|    |              |bound_to_base        |",
                                        "|    |              |bound_to_base        |"));

        RecursiveArrayNode obj = dataToObject(RecursiveArrayNode.class);

        assertThat(obj.id, is("base"));
        assertThat(obj.children.length, is(1));
        assertThat(obj.children[0].id, is(""));
        assertThat(obj.children[0].strings, arrayContaining("bound_to_base", "bound_to_base", "bound_to_base"));
    }

    @Test
    void should_parse_single_child_with_array_when_no_children_base_data() {
        givenDataTable(StringTable.from("|id  |children[]{strings[]}|",
                                        "|base|bound_to_base        |",
                                        "|    |bound_to_base        |",
                                        "|    |bound_to_base        |"));

        RecursiveArrayNode obj = dataToObject(RecursiveArrayNode.class);

        assertThat(obj.id, is("base"));
        assertThat(obj.children.length, is(1));
        assertThat(obj.children[0].id, is(nullValue()));
        assertThat(obj.children[0].strings, arrayContaining("bound_to_base", "bound_to_base", "bound_to_base"));
    }

    @Test
    void should_parse_nested_children_with_array() {
        givenDataTable(StringTable.from("|id  |next{id} |next{children[]{id}}|next{children[]{ints[]}}|",
                                        "|base|next.base|next_child1         |1                       |",
                                        "|    |         |                    |2                       |",
                                        "|    |         |next_child2         |3                       |",
                                        "|    |         |                    |4                       |"));

        RecursiveArrayNode obj = dataToObject(RecursiveArrayNode.class);

        assertThat(obj.id, is("base"));
        assertThat(obj.next.id, is("next.base"));
        assertThat(obj.next.children.length, is(2));
        assertThat(obj.next.children[0].id, is("next_child1"));
        assertThat(obj.next.children[0].ints, arrayContaining(1, 2));
        assertThat(obj.next.children[1].id, is("next_child2"));
        assertThat(obj.next.children[1].ints, arrayContaining(3, 4));
    }

    @Test
    void should_parse_children_with_mix_of_empty_and_nonempty_arrays() {
        givenDataTable(StringTable.from("|id  |children[]{id}|children[]{strings[]}|children[]{ints[]}|",
                                        "|base|child1        |                     |1                 |",
                                        "|    |              |                     |2                 |",
                                        "|    |child2        |s3                   |3                 |",
                                        "|    |              |s4                   |4                 |",
                                        "|    |              |s5                   |5                 |",
                                        "|    |              |s6                   |                  |"));

        RecursiveArrayNode obj = dataToObject(RecursiveArrayNode.class);

        assertThat(obj.id, is("base"));
        assertThat(obj.children.length, is(2));
        assertThat(obj.children[0].id, is("child1"));
        assertThat(obj.children[0].strings, arrayWithSize(0));
        assertThat(obj.children[0].ints, arrayContaining(1, 2));
        assertThat(obj.children[1].id, is("child2"));
        assertThat(obj.children[1].strings, arrayContaining("s3", "s4", "s5", "s6"));
        assertThat(obj.children[1].ints, arrayContaining(3, 4, 5));
    }

    @Test
    void should_fail_when_primitive_array_elements_dont_starts_on_first_row() {
        givenDataTable(StringTable.from("|id  |children[]{id}|children[]{strings[]}|children[]{ints[]}|",
                                        "|base|child1        |                     |1                 |",
                                        "|    |              |                     |2                 |",
                                        "|    |child2        |                     |3                 |",
                                        "|    |              |                     |4                 |",
                                        "|    |              |                     |5                 |",
                                        "|    |              |s6                   |                  |"));

        assertThat(assertThrows(RuntimeException.class, () -> dataToObject(RecursiveArrayNode.class))
                           .getMessage(), allOf(containsString("Unexpected data on other than base row (while base row is empty)"),
                                                containsString("node('children[]{strings[]}')=value('s6')")));
    }

    @Test
    void should_fail_when_base_data_for_composite_array_elements_doesnt_starts_on_first_row() {
        givenDataTable(StringTable.from("|id  |children[]{id}|children[]{ints[]}|",
                                        "|base|              |1                 |",
                                        "|    |              |2                 |",
                                        "|    |child?        |3                 |",
                                        "|    |              |4                 |",
                                        "|    |              |5                 |"));

        assertThat(assertThrows(RuntimeException.class, () -> dataToObject(RecursiveArrayNode.class))
                           .getMessage(), allOf(containsString("Unexpected data on other than base row (while base row is empty)"),
                                                containsString("node('children[]{id}')=value('child?')")));
    }


    @Getter
    @Setter
    private static class CollectionsHolder {
        private String                  id;
        private List<String>            stringList;
        private Set<Integer>            intSet;
        private CollectionsHolder       next;
        private List<CollectionsHolder> children;
    }

    @Test
    void should_parse_arrays_into_list_or_set() {
        givenDataTable(StringTable.from("|id  |stringList[]|intSet[]|next{id} |next{stringList[]}|next{children[]{id}}|next{children[]{intSet[]}}|",
                                        "|base|a           |1       |next.base|s1                |next_child1         |1                         |",
                                        "|    |b           |2       |         |s2                |                    |2                         |",
                                        "|    |            |2       |         |s3                |next_child2         |3                         |",
                                        "|    |            |1       |         |                  |                    |3                         |"));

        CollectionsHolder obj = dataToObject(CollectionsHolder.class);

        assertThat(obj.id, is("base"));
        assertThat(obj.stringList, contains("a", "b"));
        assertThat(obj.intSet, contains(1, 2));
        assertThat(obj.next.id, is("next.base"));
        assertThat(obj.next.stringList, contains("s1", "s2", "s3"));
        assertThat(obj.next.children, hasSize(2));
        assertThat(obj.next.children.get(0).id, is("next_child1"));
        assertThat(obj.next.children.get(0).intSet, contains(1, 2));
        assertThat(obj.next.children.get(1).id, is("next_child2"));
        assertThat(obj.next.children.get(1).intSet, contains(3));
    }

    @Test
    void should_parse_complex_structure_BookOrdersHistory() {
        givenDataTable(StringTable.from(
                "|orders[]{isGift}|orders[]{shipping{name}}|orders[]{shipping{address}}|orders[]{shipping{cost}}|orders[]{books[]{title}}|orders[]{books[]{author}}|orders[]{books[]{quantity}}|orders[]{books[]{price}}|",
                "|true            |Juraj                   |Home street, Prague        |3.20                    |The Rational Optimist   |M.Ridley                 |1                          |18.30                   |",
                "|                |                        |                           |                        |Civilization            |N.Ferguson               |1                          |22                      |",
                "|                |                        |                           |                        |Harry Potter            |J.K.Rowling              |3                          |11.90                   |",
                "|false           |Reception desk          |Work street, Prague        |3.30                    |Clean Code              |R.C.Martin               |1                          |29.50                   |",
                "|                |                        |                           |                        |Spec By Example         |G.Adzic                  |1                          |25.00                   |"));

        BookOrdersHistory obj = dataToObject(BookOrdersHistory.class);

        assertMyBookOrdersHistory(obj);
    }

    @Test
    void should_parse_complex_structure_BookOrdersHistory_when_compressed_headers() {
        givenDataTable(StringTable.from(
                "|orders[]{isGift}|{shipping{name}}        |{{address}}                |{{cost}}                |{books[]{title}}        |{{author}}               |{{quantity}}               |{{price}}               |",
                "|true            |Juraj                   |Home street, Prague        |3.20                    |The Rational Optimist   |M.Ridley                 |1                          |18.30                   |",
                "|                |                        |                           |                        |Civilization            |N.Ferguson               |1                          |22                      |",
                "|                |                        |                           |                        |Harry Potter            |J.K.Rowling              |3                          |11.90                   |",
                "|false           |Reception desk          |Work street, Prague        |3.30                    |Clean Code              |R.C.Martin               |1                          |29.50                   |",
                "|                |                        |                           |                        |Spec By Example         |G.Adzic                  |1                          |25.00                   |"));

        BookOrdersHistory obj = dataToObject(BookOrdersHistory.class);

        assertMyBookOrdersHistory(obj);
    }

    private void assertMyBookOrdersHistory(BookOrdersHistory obj) {
        assertThat(obj.getOrders(), hasSize(2));


        BookOrdersHistory.Order order1 = obj.getOrders().get(0);
        assertThat(order1.isGift(), is(true));
        assertShipping(order1.getShipping(),
                       "Juraj", "Home street, Prague", 3.20);

        assertThat(order1.getBooks(), arrayWithSize(3));
        assertBook(order1.getBooks()[0],
                   "The Rational Optimist", "M.Ridley", 1, 18.30);
        assertBook(order1.getBooks()[1],
                   "Civilization", "N.Ferguson", 1, 22.00);
        assertBook(order1.getBooks()[2],
                   "Harry Potter", "J.K.Rowling", 3, 11.90);


        BookOrdersHistory.Order order2 = obj.getOrders().get(1);
        assertThat(order2.isGift(), is(false));
        assertShipping(order2.getShipping(),
                       "Reception desk", "Work street, Prague", 3.30);

        assertThat(order2.getBooks(), arrayWithSize(2));
        assertBook(order2.getBooks()[0],
                   "Clean Code", "R.C.Martin", 1, 29.50);
        assertBook(order2.getBooks()[1],
                   "Spec By Example", "G.Adzic", 1, 25.00);
    }

    private void assertShipping(BookOrdersHistory.Shipping shipping, String name, String address, double cost) {
        assertThat(shipping.getName(), is(name));
        assertThat(shipping.getAddress(), is(address));
        assertThat(shipping.getCost(), is(cost));
    }

    private void assertBook(BookOrdersHistory.Book book, String title, String author, int quantity, double price) {
        assertThat(book.getTitle(), is(title));
        assertThat(book.getAuthor(), is(author));
        assertThat(book.getQuantity(), is(quantity));
        assertThat(book.getPrice(), is(price));
    }


    @Test
    void should_fail_when_compressed_header_name_doesnt_build_on_top_of_previous_column() {
        givenDataTable(StringTable.from(
                "|orders[]{isGift}|{{name}}      |{{address}}        |",
                "|true            |Juraj         |Home street, Prague|"));

        assertThat(assertThrows(RuntimeException.class, () -> dataToObject(BookOrdersHistory.class))
                           .getMessage(), containsString("Node to be parsed ('{{name}}') and previous column node ('orders[]{isGift}') do not have a common parent ('orders[]')"));
    }


    private void givenDataTable(Table table) {
        this.dataTable = table;
    }

    private <T> T dataToObject(Class<T> dataType) {
        String json = new TabularData(dataTable).asJson();
        return gson.fromJson(json, dataType);
    }

}
