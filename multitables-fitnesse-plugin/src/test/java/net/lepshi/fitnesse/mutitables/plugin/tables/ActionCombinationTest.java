package net.lepshi.fitnesse.mutitables.plugin.tables;

import fitnesse.testsystems.ExecutionResult;
import fitnesse.testsystems.TestExecutionException;
import fitnesse.testsystems.slim.SlimTestContext;
import fitnesse.testsystems.slim.Table;
import fitnesse.testsystems.slim.results.SlimExceptionResult;
import fitnesse.testsystems.slim.results.SlimTestResult;
import fitnesse.testsystems.slim.tables.SlimAssertion;
import fitnesse.testsystems.slim.tables.SlimTable;
import fitnesse.testsystems.slim.tables.SlimTableFactory;
import net.lepshi.fitnesse.mutitables.plugin.PluginContextTestAccess;
import net.lepshi.fitnesse.mutitables.plugin.TableDataHighlighter;
import net.lepshi.fitnesse.mutitables.plugin.TableException;
import net.lepshi.fitnesse.mutitables.plugin.common.StringTable;
import net.lepshi.fitnesse.mutitables.plugin.tables.ActionCombination.CombinationType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static fitnesse.testsystems.ExecutionResult.IGNORE;
import static java.lang.String.format;
import static java.util.Collections.singletonList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.Matchers.sameInstance;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class ActionCombinationTest {

    private static final CombinationType COMBINATION_TYPE = CombinationType.TEST_CASE;
    private static final String          COMBINATION_ID   = "combination_id";

    private final PluginContextTestAccess contextTestAccess = new PluginContextTestAccess();

    private Table               titleTable;
    private List<Table>         actionTables  = new ArrayList<>();
    private List<SlimAssertion> allAssertions = new ArrayList<>();
    @Mock
    private SlimTableFactory    slimTableFactory;
    @Mock
    private SlimTestContext     slimTestContext;


    @BeforeEach
    void init() {
        MockitoAnnotations.initMocks(this);
        contextTestAccess.setSlimTableFactory(slimTableFactory);
        givenTitle("|Dummy|");
    }


    @Test
    void should_provide_name_as_is() {

        assertThat(createActionCombination().getCombinationType(), is(COMBINATION_TYPE));
    }

    @Test
    void should_provide_ID_as_is() {

        assertThat(createActionCombination().getCombinationIdSupplier().get(), is(COMBINATION_ID));
    }

    @Test
    void should_provide_titleTable_as_is() {

        assertThat(createActionCombination().getTitleTable(), is(sameInstance(titleTable)));
    }

    @Test
    void should_provide_title() {
        givenTitle("|Combi|Descr|");

        assertThat(createActionCombination().getCombinationTitle(), is("Combi"));
    }

    @Test
    void should_provide_actionTables_as_is() {

        assertThat(createActionCombination().getActionTables(), is(sameInstance(actionTables)));
    }

    @Test
    void should_collect_assertions_when_single_action() throws TestExecutionException {
        givenTitle("|TC|Desc|");
        givenActionTable();

        createActionCombinationAssertions();

        verify(titleTable, never()).updateContent(anyInt(), anyInt(), any(SlimTestResult.class));
        verify(titleTable, never()).updateContent(anyInt(), anyInt(), any(SlimExceptionResult.class));
    }

    @Test
    void should_collect_assertions_when_multiple_actions() throws TestExecutionException {
        givenTitle("|TC|");
        givenActionTable();
        givenActionTable();
        givenActionTable();

        createActionCombinationAssertions();

        verify(titleTable, never()).updateContent(anyInt(), anyInt(), any(SlimTestResult.class));
        verify(titleTable, never()).updateContent(anyInt(), anyInt(), any(SlimExceptionResult.class));
    }

    @Test
    void should_ignore_failed_action_combination_when_slim_table_error() throws TestExecutionException {
        givenTitle("|TC|Desc|");
        givenActionTable();
        givenActionTable();
        SlimTable slimTable2 = givenActionTable();
        givenActionTable();
        givenActionTable();

        when(slimTable2.getAssertions()).thenThrow(new RuntimeException("slim table error"));
        allAssertions.clear();

        createActionCombinationAssertions();

        verify(slimTestContext).incrementErroredTestsCount();
        verify(titleTable, times(2)).updateContent(anyInt(), anyInt(), any(SlimTestResult.class));
        verify(titleTable, times(1)).updateContent(anyInt(), anyInt(), any(SlimExceptionResult.class));
        assertTitleHighlighting(0, IGNORE);
        assertTitleHighlighting(1, IGNORE);
        assertTitleExceptionHighlighting("TC", "slim table error");
    }

    @Test
    void should_ignore_failed_action_combination_and_highlight_when_TableException() throws TestExecutionException {
        givenTitle("|Combination Name|Desc|");
        SlimTable slimTable = givenActionTable();

        TableDataHighlighter mockHighlighter = mock(TableDataHighlighter.class);
        when(slimTable.getAssertions()).thenThrow(new TableException(mockHighlighter, "table error"));
        allAssertions.clear();

        createActionCombinationAssertions();

        verify(slimTestContext).incrementErroredTestsCount();
        verify(titleTable, times(2)).updateContent(anyInt(), anyInt(), any(SlimTestResult.class));
        verify(titleTable, times(1)).updateContent(anyInt(), anyInt(), any(SlimExceptionResult.class));
        assertTitleHighlighting(0, IGNORE);
        assertTitleHighlighting(1, IGNORE);
        assertTitleExceptionHighlighting("Combination Name", "table error");

        verify(mockHighlighter).highlight();
    }

    @Test
    void should_ignore_failed_action_combination_when_TestExecutionException() throws TestExecutionException {
        givenTitle("|TC|Desc|");
        givenActionTable();
        SlimTable slimTable = givenActionTable();
        givenActionTable();

        when(slimTable.getAssertions()).thenThrow(new TestExecutionException("test exec error"));
        allAssertions.clear();

        createActionCombinationAssertions();

        verify(titleTable, times(2)).updateContent(anyInt(), anyInt(), any(SlimTestResult.class));
        verify(titleTable, times(1)).updateContent(anyInt(), anyInt(), any(SlimExceptionResult.class));
        assertTitleHighlighting(0, IGNORE);
        assertTitleHighlighting(1, IGNORE);
        assertTitleExceptionHighlighting("TC", "Action error [action[1]@" + COMBINATION_ID + "]: test exec error");
    }


    private void givenTitle(String titleRow) {
        titleTable = StringTable.from(titleRow);
        titleTable = spy(titleTable);
        doNothing().when(titleTable).updateContent(anyInt(), anyInt(), any(SlimTestResult.class));
        doNothing().when(titleTable).updateContent(anyInt(), anyInt(), any(SlimExceptionResult.class));
    }

    private SlimTable givenActionTable() throws TestExecutionException {
        final Table actionTable = mock(Table.class);
        final int actionIdx = actionTables.size();
        actionTables.add(actionTable);
        final SlimAssertion actionAssertion = mock(SlimAssertion.class);
        allAssertions.add(actionAssertion);

        final String actionTableId = format("action[%d]@%s", actionIdx, COMBINATION_ID);
        SlimTable mockSlimTable = mock(SlimTable.class);
        when(mockSlimTable.getAssertions()).thenReturn(singletonList(actionAssertion));
        when(slimTableFactory.makeSlimTable(actionTable, actionTableId, slimTestContext)).thenReturn(mockSlimTable);

        return mockSlimTable;
    }

    private ActionCombination createActionCombination() {
        final AssertionsCustomizer noopCombiner = mock(AssertionsCustomizer.class);
        when(noopCombiner.customize(any())).thenAnswer(invocation -> invocation.getArgument(0));

        return new ActionCombination(COMBINATION_TYPE,
                                     () -> COMBINATION_ID,
                                     titleTable,
                                     actionTables,
                                     noopCombiner,
                                     slimTestContext);
    }

    private void createActionCombinationAssertions() {
        ActionCombination actionCombination = createActionCombination();
        assertThat(actionCombination.getAssertions(), is(allAssertions));
    }

    private void assertTitleHighlighting(int col, ExecutionResult cellExecutionResult) {
        final ArgumentCaptor<SlimTestResult> testResultCaptor = ArgumentCaptor.forClass(SlimTestResult.class);

        verify(titleTable).updateContent(eq(col), eq(0), testResultCaptor.capture());

        final SlimTestResult actualTestResult = testResultCaptor.getValue();
        assertThat(actualTestResult.getExecutionResult(), is(cellExecutionResult));
        assertThat(actualTestResult.getMessage(), is(nullValue()));
    }

    private void assertTitleExceptionHighlighting(String combinationName, String msgDetail) {
        final String msgPrefix = format("Failed to get assertions for %s [%s]: ",
                                        COMBINATION_TYPE.decorateTitle(combinationName), COMBINATION_ID);
        final ArgumentCaptor<SlimExceptionResult> testExceptionCaptor = ArgumentCaptor.forClass(SlimExceptionResult.class);

        verify(titleTable).updateContent(eq(0), eq(0), testExceptionCaptor.capture());

        assertThat(testExceptionCaptor.getValue().getException(), is(msgPrefix + msgDetail));
    }
}